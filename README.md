### Simulating Optical Depth For Participating Media Using Ray Tracing
This repository contains the code for my master's thesis: Simulating Optical Depth For Participating Media Using Ray Tracing\
Uses DirectX Raytracing.\
Currently a work in progress.\
\
The following link shows a recording of the latest build: [demonstration.mp4](demonstration.mp4)\
The video shows two tubes of smoke being lit up by a light source that is rotating around the z-axis.
