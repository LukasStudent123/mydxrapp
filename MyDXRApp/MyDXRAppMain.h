﻿#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "Content\Sample3DSceneRenderer.h"

// Renders Direct3D content on the screen.
namespace MyDXRApp
{
	class MyDXRAppMain
	{
	public:
		MyDXRAppMain();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();
		std::unique_ptr<Sample3DSceneRenderer> m_sceneRenderer;

	private:
		// TODO: Replace with your own content renderers.

		// Rendering loop timer.
		DX::StepTimer m_timer;
	};
}