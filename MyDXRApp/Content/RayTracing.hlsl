/*
	Source: http://intro-to-dxr.cwyman.org/
	D3D12 Raytracing Hello World example
	D3D12 Raytracing Simple Lighting example
*/
#define RAYTRACER
#include "ShaderStructures.h"

RWTexture2D<float4> outTex : register(u0); // Output texture
RWStructuredBuffer<float> opticalDepths : register(u1); // Optical Depth buffer
RWStructuredBuffer<float> vecOpticalDepths : register(u2); // vec Optical Depth buffer
RaytracingAccelerationStructure sceneAccelStruct : register(t0, space0); // Scene Acceleration Structure
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<MyDXRApp::VertexPositionColor> Vertices : register(t2, space0);
Texture2D<float4> Tex : register (t3, space0);
Texture2D<float4> TexNeg : register (t4, space0);

ConstantBuffer<MyDXRApp::RayGenData> rayData : register(b0);
ConstantBuffer<MyDXRApp::ParticlePositionData> particlePosData : register(b1);
ConstantBuffer<MyDXRApp::ValuesData> valuesData : register(b2);

struct ViewRayPayload
{
	float4 rayColor;
	float3 hitPos;
	uint hitId;
	bool end;
};

struct OpticalDepthRayPayload
{
	float3 hitPos;
	int inside;
	bool end;
	bool blocked;
};

struct VecOpticalDepthRayPayload
{
	int nbrHits;
};

struct ShadowRayPayload
{
	float opticalDepth;
};

TriangleHitGroup MyHitGroup =
{
	//"",					// Any hit
	"RayAnyHit",		// Any hit
	"RayClosestHit",	// Closest hit
};

TriangleHitGroup MyHitGroupOpticalDepth =
{
	//"",							 // Any hit
	"RayAnyHitOpticalDepth",     // Any hit
	"RayClosestHitOpticalDepth", // Closest hit
};

TriangleHitGroup MyHitGroupShadow =
{
	"",						// Any hit
	"RayClosestHitShadow",	// Closest hit
};

// From D3D12 Raytracing Simple Lighting example
// Load three 16 bit indices from a byte addressed buffer.
uint3 Load3x16BitIndices(uint offsetBytes)
{
	uint3 indices;

	// ByteAdressBuffer loads must be aligned at a 4 byte boundary.
	// Since we need to read three 16 bit indices: { 0, 1, 2 } 
	// aligned at a 4 byte boundary as: { 0 1 } { 2 0 } { 1 2 } { 0 1 } ...
	// we will load 8 bytes (~ 4 indices { a b | c d }) to handle two possible index triplet layouts,
	// based on first index's offsetBytes being aligned at the 4 byte boundary or not:
	//  Aligned:     { 0 1 | 2 - }
	//  Not aligned: { - 0 | 1 2 }
	const uint dwordAlignedOffset = offsetBytes & ~3;
	const uint2 four16BitIndices = Indices.Load2(dwordAlignedOffset);

	// Aligned: { 0 1 | 2 - } => retrieve first three 16bit indices
	if (dwordAlignedOffset == offsetBytes)
	{
		indices.x = four16BitIndices.x & 0xffff;
		indices.y = (four16BitIndices.x >> 16) & 0xffff;
		indices.z = four16BitIndices.y & 0xffff;
	}
	else // Not aligned: { - 0 | 1 2 } => retrieve last three 16bit indices
	{
		indices.x = (four16BitIndices.x >> 16) & 0xffff;
		indices.y = four16BitIndices.y & 0xffff;
		indices.z = (four16BitIndices.y >> 16) & 0xffff;
	}

	return indices;
}

// From D3D12 Raytracing Simple Lighting example
// Diffuse lighting calculation.
float3 CalculateDiffuseLighting(float3 hitPosition, float3 normal, float3 cubeColor)
{
	float3 pixelToLight = normalize(rayData.lightPos - hitPosition);

	// Diffuse contribution.
	float fNDotL = max(0.0f, dot(pixelToLight, normal));

	return cubeColor * rayData.lightDiffuseColor * fNDotL;
}

// From D3D12 Raytracing Simple Lighting example
// Retrieve hit world position.
float3 HitWorldPosition()
{
	return WorldRayOrigin() + RayTCurrent() * WorldRayDirection();
}

[shader("raygeneration")]
void RayGenerationOpticalDepth()
{
	uint particleIndex = DispatchRaysIndex().x;
	float3 particlePos = particlePosData.positions[particleIndex].xyz;
	uint particleId = (uint) particlePosData.positions[particleIndex].w;
	//float3 direction = rayData.lightPos - particlePos;
	float3 direction = rayData.lightPos;

	RayDesc ray;
	ray.Origin = particlePos;
	ray.Direction = direction;
	ray.TMin = 1e-7f;
	ray.TMax = 1e+38f;

	OpticalDepthRayPayload p;
	p.hitPos = float3(0.0f, 0.0f, 0.0f);
	p.inside = 0;
	p.end = false;
	p.blocked = false;

	float opticalDepth = 0.0f;
	int inside = 1;
	float3 entryPoint = particlePos;
	bool isEntryPointSet = true;

	uint recDepth = 0;
	while (!p.end  && recDepth < 30)
	{
		p.inside = 0;

		if (p.inside != 0)
		{
			//ray.TMax = 0.2f + 0.01f;
		}
		else
		{
			ray.TMax = 1e+38f;
		}

		TraceRay(sceneAccelStruct, RAY_FLAG_FORCE_OPAQUE,   // ~skip any hit shaders
		//TraceRay(sceneAccelStruct, RAY_FLAG_NONE,
		//~2,												// Do not include the cube
			0xFF,
			0, 1, 0, ray, p);

		inside += p.inside;

		if (p.blocked)
		{
			opticalDepth = 999999.0f;
			break;
		}

		/*if (p.inside == -1)
		{
			opticalDepth += length(p.hitPos - ray.Origin);
		}*/

		if (!isEntryPointSet)
		{
			entryPoint = p.hitPos;
			isEntryPointSet = true;
		}

		if (inside == 0)
		{
			opticalDepth += length(p.hitPos - entryPoint);
			isEntryPointSet = false;
		}

		ray.Origin = p.hitPos;
		recDepth += 1;
	}
	/*if (recDepth == 30)
	{
		opticalDepth = 999999.0f;
	}*/
	opticalDepths[particleId] = opticalDepth;
}

[shader("raygeneration")]
void RayGenerationOpticalDepth2()
{
	//uint particleIndex = DispatchRaysIndex().x;
	uint particleIndex = DispatchRaysIndex().x * PARTICLE_GROUP_AMOUNT + valuesData.currentParticleGroup;
	float3 particlePos = particlePosData.positions[particleIndex].xyz;
	uint particleId = (uint) particlePosData.positions[particleIndex].w;
	//float3 direction = rayData.lightPos - particlePos;
	float3 direction = rayData.lightPos;

	RayDesc ray;
	ray.Origin = particlePos;
	ray.Direction = direction;
	ray.TMin = 1e-7f;
	ray.TMax = 1e+38f;

	VecOpticalDepthRayPayload p;
	p.nbrHits = 0;

	TraceRay(sceneAccelStruct, RAY_FLAG_SKIP_CLOSEST_HIT_SHADER,
		//0xFF,
		//~2,	// ignore the particle quads
		~2 & ~4,	// ignore the particle quads and cube
		0, 1, 0, ray, p);
}

[shader("raygeneration")]
void RayGeneration()
{
	uint2 curPixel = DispatchRaysIndex().xy;
	uint2 totalPixels = DispatchRaysDimensions().xy;
	float2 pixelCenter = (curPixel + float2(0.5f, 0.5f)) / totalPixels;
	float2 ndc = float2(2, -2) * pixelCenter + float2(-1, 1);
	ndc.y = -ndc.y;
	float3 pixelRayDir = ndc.x * rayData.wsCamU + ndc.y * rayData.wsCamV + rayData.wsCamW;

	float3 dir = -normalize(pixelRayDir);

	RayDesc ray;
	ray.Origin = rayData.wsCamPos;
	ray.Direction = dir;
	ray.TMin = 0.001f;
	ray.TMax = 1e+38f;

	ViewRayPayload payload;
	//payload.rayColor = float3(0.0f, 0.0f, 0.0f);
	payload.rayColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
	payload.hitPos = rayData.wsCamPos;
	payload.hitId = 0;
	payload.end = false;
	uint recDepth = 0;
	while (!payload.end && recDepth < 60)
	{
		ViewRayPayload p;
		//p.rayColor = payload.rayColor;
		p.hitPos = payload.hitPos;
		p.hitId = payload.hitId;
		p.end = payload.end;

		TraceRay(sceneAccelStruct,
			//RAY_FLAG_FORCE_OPAQUE,   // ~skip any hit shaders
			//|| RAY_FLAG_CULL_BACK_FACING_TRIANGLES,
			RAY_FLAG_NONE,
			//0xFF,
			//~1,	// ignore spheres
			~1 & ~4,	// ignore spheres and cube
			0, 1, 0, ray, p);

		payload.hitPos = p.hitPos;
		payload.hitId = p.hitId;
		payload.end = p.end;

		ray.Origin = p.hitPos;

		if (!payload.end)
		{
			float opticalDepth = opticalDepths[p.hitId];
			//float opticalDepth = vecOpticalDepths[p.hitId * 2 * 30];
			if (valuesData.basic_mode)
			{
				payload.rayColor.rgb = payload.rayColor.rgb + (1.0f - payload.rayColor.a) * p.rayColor.a * (p.rayColor.rgb + rayData.lightAmbientColor);
			}
			else if (valuesData.simple_shadow_mode)
			{
				payload.rayColor.rgb = payload.rayColor.rgb + (1.0f - payload.rayColor.a) * p.rayColor.a * (p.rayColor.rgb / exp(opticalDepth / 8.0f) + rayData.lightAmbientColor);
			}
			else if (valuesData.full_optical_depth_mode)
			{
				payload.rayColor.rgb = payload.rayColor.rgb + (1.0f - payload.rayColor.a) * p.rayColor.a * (p.rayColor.rgb / exp(opticalDepth / 8.0f) + rayData.lightAmbientColor);
			}
			payload.rayColor.a = p.rayColor.a;
		}
		else
		{
			//payload.rayColor += p.rayColor;
			payload.rayColor.rgb = payload.rayColor.rgb + (1.0f - payload.rayColor.a) * p.rayColor.a * p.rayColor.rgb;
		}

		//payload.rayColor = p.rayColor;

		ray.Origin = payload.hitPos;
		ray.TMax = 1e+38f;

		if (p.rayColor.r != 0.0f || p.rayColor.g != 0.0f || p.rayColor.b != 0.0f)
		{
			recDepth += 1;
		}
	}

	//outTex[curPixel] = float4(payload.rayColor, 1.0f);
	outTex[curPixel] = payload.rayColor;
}

[shader("miss")]
void RayMiss(inout ViewRayPayload data)
{
	//data.rayColor = float3(0.0f, 0.2f, 0.4f);
	data.rayColor = float4(0.0f, 0.2f, 0.4f, 1.0f);
	data.end = true;
}

[shader("anyhit")]
void RayAnyHit(inout ViewRayPayload data,
	BuiltInTriangleIntersectionAttributes attribs)
{
	if (HitKind() == HIT_KIND_TRIANGLE_BACK_FACE)
	{
		//IgnoreHit();
	}
}

[shader("closesthit")]
void RayClosestHit(inout ViewRayPayload data,
	BuiltInTriangleIntersectionAttributes attribs)
{
	// Get the base index of the triangle's first 16 bit index.
	uint indexSizeInBytes = 2;
	uint indicesPerTriangle = 3;
	uint triangleIndexStride = indicesPerTriangle * indexSizeInBytes;
	uint baseIndex = PrimitiveIndex() * triangleIndexStride;

	// Load up 3 16 bit indices for the triangle.
	const uint3 indices = Load3x16BitIndices(baseIndex);

	uint index0 = indices[0];
	uint index1 = indices[1];
	uint index2 = indices[2];

	/*float2 vertexTexCoords[3] = {
		Vertices[index0].texCoords,
		Vertices[index1].texCoords,
		Vertices[index2].texCoords
	};*/

	float2 vertexTexCoords[3];
	if (PrimitiveIndex() == 0)
	{
		vertexTexCoords[0] = Vertices[60].texCoords;
		vertexTexCoords[1] = Vertices[61].texCoords;
		vertexTexCoords[2] = Vertices[62].texCoords;
	}
	else if (PrimitiveIndex() == 1)
	{
		vertexTexCoords[0] = Vertices[61].texCoords;
		vertexTexCoords[1] = Vertices[63].texCoords;
		vertexTexCoords[2] = Vertices[62].texCoords;
	}
	else
	{
		vertexTexCoords[0] = Vertices[index0].texCoords;
		vertexTexCoords[1] = Vertices[index1].texCoords;
		vertexTexCoords[2] = Vertices[index2].texCoords;
	}
	//0, 1, 2   1, 3, 2

	float3 hitPos = HitWorldPosition();

	float2 texcoords = vertexTexCoords[0] + attribs.barycentrics.x * (vertexTexCoords[1] - vertexTexCoords[0]) + attribs.barycentrics.y * (vertexTexCoords[2] - vertexTexCoords[0]);

	/*float4 tex = Tex[texcoords * 256];
	data.rayColor = tex.rgb * tex.a;
	return;*/

	if (InstanceID() == NBR_PARTICLES * 2)
	{
		RayDesc ray;
		ray.Origin = hitPos;
		ray.Direction = rayData.lightPos - hitPos;
		ray.TMin = 0.001f;
		ray.TMax = 1e+38f;

		ShadowRayPayload p;
		p.opticalDepth = 0.0f;

		TraceRay(sceneAccelStruct,
			RAY_FLAG_FORCE_OPAQUE,   // ~skip any hit shaders
			//0xFF,
			//~2,	//ignore particle quads
			~2 & ~4,	//ignore particle quads and cube
			2, 2, 1, ray, p);

		float3 normal = float3(0.0f, 1.0f, 0.0f);
		float3 color = float3(1.0f, 0.0f, 0.0f);

		//data.rayColor += CalculateDiffuseLighting(hitPos, normal, color) / (p.opticalDepth + 1.0f) + float3(0.2f, 0.0f, 0.0f);
		if (valuesData.basic_mode)
		{
			data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) + float3(0.2f, 0.0f, 0.0f);
		}
		else if (valuesData.simple_shadow_mode)
		{
			if (p.opticalDepth == 0.0f)
			{
				data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) + float3(0.2f, 0.0f, 0.0f);
			}
			else
			{
				data.rayColor.rgb = float3(0.2f, 0.0f, 0.0f);
			}
		}
		else if (valuesData.full_optical_depth_mode)
		{
			data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) / exp(p.opticalDepth / 8.0f) + float3(0.2f, 0.0f, 0.0f);
		}
		data.rayColor.a = 1.0f;

		data.hitPos = hitPos;
		data.hitId = InstanceID();
		data.end = true;
	}
	else if (InstanceID() == NBR_PARTICLES * 2 + 1)
	{
		RayDesc ray;
		ray.Origin = hitPos;
		ray.Direction = rayData.lightPos - hitPos;
		ray.TMin = 0.001f;
		ray.TMax = 1e+38f;

		ShadowRayPayload p;
		p.opticalDepth = 0.0f;

		TraceRay(sceneAccelStruct,
			RAY_FLAG_FORCE_OPAQUE,   // ~skip any hit shaders
			//0xFF,
			//~2,	//ignore particle quads
			~2 & ~4,	//ignore particle quads and cube
			2, 2, 1, ray, p);

		//float3 normal = float3(0.0f, 1.0f, 0.0f);
		float3 vertexNormals[3];

		if (PrimitiveIndex() == 0)
		{
			vertexNormals[0] = Vertices[0 + 68].normal;
			vertexNormals[1] = Vertices[2 + 68].normal;
			vertexNormals[2] = Vertices[1 + 68].normal;
		}
		else if (PrimitiveIndex() == 1)
		{
			vertexNormals[0] = Vertices[1 + 68].normal;
			vertexNormals[1] = Vertices[2 + 68].normal;
			vertexNormals[2] = Vertices[3 + 68].normal;
		}
		else if (PrimitiveIndex() == 2)
		{
			vertexNormals[0] = Vertices[4 + 68].normal;
			vertexNormals[1] = Vertices[5 + 68].normal;
			vertexNormals[2] = Vertices[6 + 68].normal;
		}
		else if (PrimitiveIndex() == 3)
		{
			vertexNormals[0] = Vertices[5 + 68].normal;
			vertexNormals[1] = Vertices[7 + 68].normal;
			vertexNormals[2] = Vertices[6 + 68].normal;
		}
		else if (PrimitiveIndex() == 4)
		{
			vertexNormals[0] = Vertices[8 + 68].normal;
			vertexNormals[1] = Vertices[9 + 68].normal;
			vertexNormals[2] = Vertices[11 + 68].normal;
		}
		else if (PrimitiveIndex() == 5)
		{
			vertexNormals[0] = Vertices[8 + 68].normal;
			vertexNormals[1] = Vertices[11 + 68].normal;
			vertexNormals[2] = Vertices[10 + 68].normal;
		}
		else if (PrimitiveIndex() == 6)
		{
			vertexNormals[0] = Vertices[12 + 68].normal;
			vertexNormals[1] = Vertices[14 + 68].normal;
			vertexNormals[2] = Vertices[15 + 68].normal;
		}
		else if (PrimitiveIndex() == 7)
		{
			vertexNormals[0] = Vertices[12 + 68].normal;
			vertexNormals[1] = Vertices[15 + 68].normal;
			vertexNormals[2] = Vertices[13 + 68].normal;
		}
		else if (PrimitiveIndex() == 8)
		{
			vertexNormals[0] = Vertices[16 + 68].normal;
			vertexNormals[1] = Vertices[18 + 68].normal;
			vertexNormals[2] = Vertices[19 + 68].normal;
		}
		else if (PrimitiveIndex() == 9)
		{
			vertexNormals[0] = Vertices[16 + 68].normal;
			vertexNormals[1] = Vertices[19 + 68].normal;
			vertexNormals[2] = Vertices[17 + 68].normal;
		}
		else if (PrimitiveIndex() == 10)
		{
			vertexNormals[0] = Vertices[20 + 68].normal;
			vertexNormals[1] = Vertices[21 + 68].normal;
			vertexNormals[2] = Vertices[23 + 68].normal;
		}
		else if (PrimitiveIndex() == 11)
		{
			vertexNormals[0] = Vertices[20 + 68].normal;
			vertexNormals[1] = Vertices[23 + 68].normal;
			vertexNormals[2] = Vertices[22 + 68].normal;
		}
		float3 normal = vertexNormals[0] + attribs.barycentrics.x * (vertexNormals[1] - vertexNormals[0]) + attribs.barycentrics.y * (vertexNormals[2] - vertexNormals[0]);
		//float3 normal = vertexNormals[0];
		float3 color = float3(1.0f, 0.0f, 0.0f);

		//data.rayColor += CalculateDiffuseLighting(hitPos, normal, color) / (p.opticalDepth + 1.0f) + float3(0.2f, 0.0f, 0.0f);
		if (valuesData.basic_mode)
		{
			data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) + float3(0.2f, 0.0f, 0.0f);
		}
		else if (valuesData.simple_shadow_mode)
		{
			if (p.opticalDepth == 0.0f)
			{
				data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) + float3(0.2f, 0.0f, 0.0f);
			}
			else
			{
				data.rayColor.rgb = float3(0.2f, 0.0f, 0.0f);
			}
		}
		else if (valuesData.full_optical_depth_mode)
		{
			data.rayColor.rgb = CalculateDiffuseLighting(hitPos, normal, color) / exp(p.opticalDepth / 8.0f) + float3(0.2f, 0.0f, 0.0f);
		}
		data.rayColor.a = 1.0f;

		data.hitPos = hitPos;
		data.hitId = InstanceID();
		data.end = true;
		//data.rayColor = float3(0.0f, 0.3f, 0.0f);
	}
	else if (InstanceID() >= NBR_PARTICLES)
	{
		data.hitPos = hitPos;
		data.hitId = InstanceID() - NBR_PARTICLES;
		uint texFrame = (valuesData.texFrame + data.hitId) % 128;
		texcoords = texcoords * 256;
		texcoords.x += 256 * (texFrame % 16);
		texcoords.y += 256 * (texFrame / 16);
		float4 tex = Tex[texcoords];
		float4 texNeg = TexNeg[texcoords];
		//data.rayColor += tex.rgb * tex.a;
		//data.rayColor += float3(0.5f, 0.5f, 0.5f) * (1.0f / 10.0f);
		if (tex.a == 0.0f)
		{
			data.rayColor.rgb = float3(0.0f, 0.0f, 0.0f);
			data.rayColor.a = 0.0f;
			//data.rayColor = float3(0.2f, 0.2f, 0.2f);
			return;
		}

		// source: https://viktorpramberg.com/smoke-lighting
		// Create a matrix based on the cameras axis, since the particle will always be looking at it.
		float3x3 tangentSpaceMatrix;
		tangentSpaceMatrix[0] = rayData.wsCamU.xyz;
		tangentSpaceMatrix[1] = -rayData.wsCamV.xyz;
		tangentSpaceMatrix[2] = -rayData.wsCamW.xyz;

		// Need to account for the particle's rotation
		float sx = sin(valuesData.particleRot.x);
		float cx = cos(valuesData.particleRot.x);
		float3x3 rotationx;
		rotationx[0] = float3(1, 0, 0);
		rotationx[1] = float3(0, cx, -sx);
		rotationx[2] = float3(0, sx, cx);

		float sy = sin(valuesData.particleRot.y);
		float cy = cos(valuesData.particleRot.y);
		float3x3 rotationy;
		rotationy[0] = float3(cy, -0, sy);
		rotationy[1] = float3(0, 1, 0);
		rotationy[2] = float3(-sy, 0, cy);

		float sz = sin(valuesData.particleRot.z);
		float cz = cos(valuesData.particleRot.z);
		float3x3 rotationz;
		rotationz[0] = float3(cz, -sz, 0);
		rotationz[1] = float3(sz, cz, 0);
		rotationz[2] = float3(0, 0, 1);

		tangentSpaceMatrix = mul(mul(mul(rotationx, rotationy), rotationz), tangentSpaceMatrix);

		float3 lightDir = hitPos - rayData.lightPos.xyz;
		float3 lightDirTS = mul(tangentSpaceMatrix, lightDir);

		float hMap = (lightDirTS.x > 0.0f) ? tex.r : texNeg.r;
		float vMap = (lightDirTS.y > 0.0f) ? tex.g : texNeg.g;
		float dMap = (lightDirTS.z > 0.0f) ? tex.b : texNeg.b;
		//float3 lightMap = float3(hMap, vMap, dMap);
		float lightMap = hMap * lightDirTS.x * lightDirTS.x + vMap * lightDirTS.y * lightDirTS.y + dMap * lightDirTS.z * lightDirTS.z;

		//data.rayColor = rayData.lightDiffuseColor.rgb * dot(lightDirTS, tex.rgb) * tex.a;
		//data.rayColor = rayData.lightDiffuseColor.rgb * dot(lightDirTS, lightMap);
		//data.rayColor += rayData.lightDiffuseColor.rgb * lightMap * tex.a * 0.1f;
		//data.rayColor = rayData.lightDiffuseColor.rgb * lightMap * tex.a * 0.025f;
		data.rayColor.rgb = rayData.lightDiffuseColor.rgb * lightMap * tex.a;

		//data.rayColor.a = 0.036056f; //(250 particles)
		//data.rayColor.a = 0.028618f; //(500 particles)
		data.rayColor.a = 0.025f; //base opacity (750 particles)
		//data.rayColor.a = 0.022714f; //(1000 particles)
		//data.rayColor.a = 0.021086f; //(1250 particles)
		//data.rayColor.a = 0.019843f; //(1500 particles)

		//data.rayColor = tex.rgb * tex.a;
		//if (texcoords.x > 0.5f || texcoords.y > 0.5f)
		//if (PrimitiveIndex() == 0 || PrimitiveIndex() == 1)
		//if (HitKind() == HIT_KIND_TRIANGLE_FRONT_FACE)
		/*if (valuesData.texFrame == 0)
		{
			data.rayColor = float3(1.0f, 0.0f, 0.0f);
		}
		else
		{
			data.rayColor = float3(0.0f, 1.0f, 0.0f);
		}*/
		//data.rayColor = valuesData.particleRot;
		//data.rayColor = float3(texcoords.x, texcoords.y, 0.0f);
		//data.end = true;
	}
	else
	{
		data.hitPos = hitPos;
		data.hitId = InstanceID();
		//data.rayColor += float3(0.1f, 0.1f, 0.1f);
		data.rayColor.rgb = float3(0.0f, 0.3f, 0.0f);
		data.rayColor.a = 0.1f;
	}
}

/*[shader("miss")]
void RayMissOpticalDepth(inout OpticalDepthRayPayload data)
{
	data.end = true;
}*/

[shader("miss")]
void RayMissOpticalDepth(inout VecOpticalDepthRayPayload data)
{

}

[shader("anyhit")]
void RayAnyHitOpticalDepth(inout VecOpticalDepthRayPayload data,
	BuiltInTriangleIntersectionAttributes attribs)
{
	uint particleId = DispatchRaysIndex().x * PARTICLE_GROUP_AMOUNT + valuesData.currentParticleGroup;
	//int baseIdx = DispatchRaysIndex().x * 2 * OPTICAL_DEPTH_CUTOFF_POINT;
	int baseIdx = particleId * 2 * OPTICAL_DEPTH_CUTOFF_POINT;

	if (InstanceID() == NBR_PARTICLES * 2 || InstanceID() == NBR_PARTICLES * 2 + 1)
	{
		vecOpticalDepths[baseIdx + (2 * OPTICAL_DEPTH_CUTOFF_POINT) - 2] = 999999.0f;
		AcceptHitAndEndSearch();
	}
	if (data.nbrHits >= OPTICAL_DEPTH_CUTOFF_POINT)
	{
		AcceptHitAndEndSearch();
	}

	vecOpticalDepths[baseIdx + (data.nbrHits * 2)] = RayTCurrent();
	if (HitKind() == HIT_KIND_TRIANGLE_FRONT_FACE)
	{
		vecOpticalDepths[baseIdx + (data.nbrHits * 2) + 1] = 1.0f;
	}
	else if (HitKind() == HIT_KIND_TRIANGLE_BACK_FACE)
	{
		vecOpticalDepths[baseIdx + (data.nbrHits * 2) + 1] = -1.0f;
	}
	data.nbrHits += 1;
	IgnoreHit();
}

[shader("closesthit")]
void RayClosestHitOpticalDepth(inout OpticalDepthRayPayload data,
	BuiltInTriangleIntersectionAttributes attribs)
{
	if (InstanceID() == NBR_PARTICLES * 2 || InstanceID() == NBR_PARTICLES * 2 + 1)
	{
		data.end = true;
		data.blocked = true;
	}
	else
	{
		data.hitPos = HitWorldPosition();
		if (HitKind() == HIT_KIND_TRIANGLE_FRONT_FACE)
		{
			data.inside++;
			//data.inside--;
		}
		else if (HitKind() == HIT_KIND_TRIANGLE_BACK_FACE)
		{
			data.inside--;
			//data.inside++;
		}
	}
}

[shader("miss")]
void RayMissShadow(inout ShadowRayPayload data)
{
	data.opticalDepth = 0.0f;
}

[shader("closesthit")]
void RayClosestHitShadow(inout ShadowRayPayload data,
	BuiltInTriangleIntersectionAttributes attribs)
{
	if (InstanceID() == NBR_PARTICLES * 2 || InstanceID() == NBR_PARTICLES * 2 + 1)
	{
		data.opticalDepth = 999999.0f;
	}
	else
	{
		data.opticalDepth = opticalDepths[InstanceID()];
	}
}