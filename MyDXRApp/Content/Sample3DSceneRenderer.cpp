﻿#include "pch.h"
#include "Sample3DSceneRenderer.h"

#include "..\Common\DirectXHelper.h"
#include <ppltasks.h>
#include <synchapi.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>

using namespace MyDXRApp;

using namespace Concurrency;
using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace Windows::UI::ViewManagement;

// Indices into the application state map.
Platform::String^ AngleKey = "Angle";
Platform::String^ TrackingKey = "Tracking";

// Loads vertex and pixel shaders from files and instantiates the cube geometry.
Sample3DSceneRenderer::Sample3DSceneRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
	m_loadingComplete(false),
	m_radiansPerSecond(XM_PIDIV4),	// rotate 45 degrees per second
	m_angle(0),
	m_tracking(false),
	m_mappedConstantBuffer(nullptr),
	m_deviceResources(deviceResources)
{
	LoadState();
	ZeroMemory(&m_constantBufferData, sizeof(m_constantBufferData));

	CreateDeviceDependentResources();
	CreateWindowSizeDependentResources();
}

Sample3DSceneRenderer::~Sample3DSceneRenderer()
{
	m_constantBuffer->Unmap(0, nullptr);
	m_mappedConstantBuffer = nullptr;
}

void Sample3DSceneRenderer::CreateDeviceDependentResources()
{
	auto d3dDevice = m_deviceResources->GetD3DDevice();
	DX::ThrowIfFailed(d3dDevice->QueryInterface(IID_PPV_ARGS(&m_dxrDevice)));

	//auto commandList = m_deviceResources->GetCommandList();
	//DX::ThrowIfFailed(commandList->QueryInterface(IID_PPV_ARGS(&m_commandList)));

	m_gpuTimer.RestoreDevice(m_deviceResources->GetD3DDevice(), m_deviceResources->GetCommandQueue(), DX::c_frameCount);

	D3D12_FEATURE_DATA_D3D12_OPTIONS5 data{};
	DX::ThrowIfFailed(d3dDevice->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &data, sizeof(data)));
	if (data.RaytracingTier == D3D12_RAYTRACING_TIER_NOT_SUPPORTED) {
		//throw std::runtime_error("Raytracing not supported on device");
		DX::ThrowIfFailed(E_FAIL);
	}

	/*// Create a command list.
	DX::ThrowIfFailed(m_dxrDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_pipelineState.Get(), IID_PPV_ARGS(&m_commandList)));
	NAME_D3D12_OBJECT(m_commandList);
	auto commandList = m_deviceResources->GetCommandList();
	DX::ThrowIfFailed(commandList->QueryInterface(IID_PPV_ARGS(&m_commandList)));*/

	// Repurposed from https://github.com/microsoft/DirectX-Graphics-Samples/blob/master/Samples/Desktop/D3D12Raytracing/src/D3D12RaytracingHelloWorld/D3D12RaytracingHelloWorld.cpp
	{
		// Global Root Signature
		// This is a root signature that is shared across all raytracing shaders invoked during a DispatchRays() call.
		{
			CD3DX12_DESCRIPTOR_RANGE ranges[2]; // Perfomance TIP: Order from most frequent to least frequent.
			//ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);  // 1 output texture
			//ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 2, 0);  // 2 output textures
			ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 3, 0);  // 3 output textures
			ranges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 4, 1);  // 2 static index and vertex buffers.

			//CD3DX12_ROOT_PARAMETER rootParameters[4];
			//CD3DX12_ROOT_PARAMETER rootParameters[5];
			CD3DX12_ROOT_PARAMETER rootParameters[6];
			rootParameters[0].InitAsDescriptorTable(1, &ranges[0]);
			rootParameters[1].InitAsShaderResourceView(0);
			rootParameters[2].InitAsConstantBufferView(0);
			rootParameters[3].InitAsDescriptorTable(1, &ranges[1]);
			//rootParameters[4].InitAsDescriptorTable(1, &ranges[1]);
			rootParameters[4].InitAsConstantBufferView(1);
			rootParameters[5].InitAsConstantBufferView(2);
			CD3DX12_ROOT_SIGNATURE_DESC globalRootSignatureDesc(ARRAYSIZE(rootParameters), rootParameters);

			//auto device = m_deviceResources->GetD3DDevice();
			ComPtr<ID3DBlob> blob;
			ComPtr<ID3DBlob> error;

			DX::ThrowIfFailed(D3D12SerializeRootSignature(&globalRootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &blob, &error));
			DX::ThrowIfFailed(m_dxrDevice->CreateRootSignature(1, blob->GetBufferPointer(), blob->GetBufferSize(), IID_PPV_ARGS(&m_globalRootSignature)));
		}
		// Local Root Signature
		// This is a root signature that enables a shader to have unique arguments that come from shader tables.
		/* {
			CD3DX12_ROOT_PARAMETER parameters[1];

			auto size = ((sizeof(m_cubeCB) - 1) / sizeof(UINT32) + 1);
			parameters[0].InitAsConstants(size, 1, 0);
			//parameters[0].InitAsConstantBufferView(0);
			parameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			CD3DX12_ROOT_SIGNATURE_DESC descRootSignature(ARRAYSIZE(parameters), parameters);
			descRootSignature.Flags = D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;

			ComPtr<ID3DBlob> pSignature;
			ComPtr<ID3DBlob> pError;
			DX::ThrowIfFailed(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
			DX::ThrowIfFailed(m_dxrDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_localRootSignature)));
		}*/
	}

	auto createRTTask = DX::ReadDataAsync(L"RayTracing.cso").then([this](std::vector<byte>& fileData) {
		m_rayTracer = fileData;
		});

	auto createRTTask2 = DX::ReadDataAsync(L"ComputeShader.cso").then([this](std::vector<byte>& fileData) {
		m_computeShader = fileData;
		});

	auto createRTTask3 = DX::ReadDataAsync(L"ClearComputeShader.cso").then([this](std::vector<byte>& fileData) {
		m_clearComputeShader = fileData;
		});

	auto fileTask = create_task(KnownFolders::GetFolderForUserAsync(nullptr /* current user */, KnownFolderId::PicturesLibrary));

	auto fileTask2 = (fileTask).then([this](StorageFolder^ picturesFolder)
			{
				return picturesFolder->CreateFileAsync("performanceStats.txt", CreationCollisionOption::ReplaceExisting);
			});

	auto fileTask3 = (fileTask2).then([this](StorageFile^ file)
		{
			m_fileComplete = true;
		});

	auto fileTask_2 = create_task(KnownFolders::GetFolderForUserAsync(nullptr /* current user */, KnownFolderId::PicturesLibrary));

	auto fileTask2_2 = (fileTask_2).then([this](StorageFolder^ picturesFolder)
		{
			return picturesFolder->CreateFileAsync("performanceStats.csv", CreationCollisionOption::ReplaceExisting);
		});

	auto fileTask2_3 = (fileTask2_2).then([this](StorageFile^ file)
		{
			m_fileComplete2 = true;
		});

	auto createPipelineStateTask = (createRTTask && createRTTask2 && createRTTask3).then([this]() {
		// Repurposed from https://github.com/microsoft/DirectX-Graphics-Samples/blob/master/Samples/Desktop/D3D12Raytracing/src/D3D12RaytracingHelloWorld/D3D12RaytracingHelloWorld.cpp
		{
			// Create 7 subobjects that combine into a RTPSO:
			// Subobjects need to be associated with DXIL exports (i.e. shaders) either by way of default or explicit associations.
			// Default association applies to every exported shader entrypoint that doesn't have any of the same type of subobject associated with it.
			// This simple sample utilizes default shader association except for local root signature subobject
			// which has an explicit association specified purely for demonstration purposes.
			// 1 - DXIL library
			// 1 - Triangle hit group
			// 1 - Shader config
			// 2 - Local root signature and association
			// 1 - Global root signature
			// 1 - Pipeline config


			// DXIL library
			// This contains the shaders and their entrypoints for the state object.
			// Since shaders are not considered a subobject, they need to be passed in via DXIL library subobjects.

			// Define which shader exports to surface from the library.
			// If no shader exports are defined for a DXIL library subobject, all shaders will be surfaced.
			// In this sample, this could be omitted for convenience since the sample uses all shaders in the library. 
			D3D12_EXPORT_DESC raygen{};
			raygen.Name = c_raygenShaderName;
			raygen.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC opticalDepthRaygen{};
			opticalDepthRaygen.Name = c_opticalDepthRaygenShaderName;
			opticalDepthRaygen.Flags = D3D12_EXPORT_FLAG_NONE;

			D3D12_EXPORT_DESC closesthit{};
			closesthit.Name = c_closestHitShaderName;
			closesthit.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC miss{};
			miss.Name = c_missShaderName;
			miss.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC anyhit{};
			anyhit.Name = c_anyHitShaderName;
			anyhit.Flags = D3D12_EXPORT_FLAG_NONE;

			D3D12_EXPORT_DESC missOpticalDepth{};
			missOpticalDepth.Name = c_missOpticalDepthShaderName;
			missOpticalDepth.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC closesthitOpticalDepth{};
			closesthitOpticalDepth.Name = c_closestHitOpticalDepthShaderName;
			closesthitOpticalDepth.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC anyhitOpticalDepth{};
			anyhitOpticalDepth.Name = c_anyHitOpticalDepthShaderName;
			anyhitOpticalDepth.Flags = D3D12_EXPORT_FLAG_NONE;

			D3D12_EXPORT_DESC missShadow{};
			missShadow.Name = c_missShadowShaderName;
			missShadow.Flags = D3D12_EXPORT_FLAG_NONE;
			D3D12_EXPORT_DESC closesthitShadow{};
			closesthitShadow.Name = c_closestHitShadowShaderName;
			closesthitShadow.Flags = D3D12_EXPORT_FLAG_NONE;

			std::vector<D3D12_EXPORT_DESC> exports;
			exports.resize(10);
			exports[0] = raygen;
			exports[1] = opticalDepthRaygen;
			exports[2] = closesthit;
			exports[3] = miss;
			exports[4] = anyhit;
			exports[5] = missOpticalDepth;
			exports[6] = closesthitOpticalDepth;
			exports[7] = anyhitOpticalDepth;
			exports[8] = missShadow;
			exports[9] = closesthitShadow;

			D3D12_SHADER_BYTECODE libdxil = CD3DX12_SHADER_BYTECODE(&m_rayTracer[0], m_rayTracer.size());
			D3D12_DXIL_LIBRARY_DESC lib{};
			lib.DXILLibrary = libdxil;
			lib.NumExports = 10;
			lib.pExports = exports.data();
			D3D12_STATE_SUBOBJECT libObj{};
			libObj.Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
			libObj.pDesc = &lib;

			// Procedural primitive hit group
			// A hit group specifies closest hit, any hit and intersection shaders to be executed when a ray intersects the geometry's triangle/AABB.
			// In this sample, we only use procedural primitive geometry with a closest hit shader, so others are not set.
			D3D12_HIT_GROUP_DESC hitGroup{};
			hitGroup.HitGroupExport = c_hitGroupName;
			hitGroup.Type = D3D12_HIT_GROUP_TYPE_TRIANGLES;
			hitGroup.ClosestHitShaderImport = c_closestHitShaderName;
			hitGroup.AnyHitShaderImport = c_anyHitShaderName;
			D3D12_STATE_SUBOBJECT hitGroupObj{};
			hitGroupObj.Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
			hitGroupObj.pDesc = &hitGroup;

			D3D12_HIT_GROUP_DESC hitGroupOpticalDepth{};
			hitGroupOpticalDepth.HitGroupExport = c_hitGroupOpticalDepthName;
			hitGroupOpticalDepth.Type = D3D12_HIT_GROUP_TYPE_TRIANGLES;
			hitGroupOpticalDepth.ClosestHitShaderImport = c_closestHitOpticalDepthShaderName;
			hitGroupOpticalDepth.AnyHitShaderImport = c_anyHitOpticalDepthShaderName;
			D3D12_STATE_SUBOBJECT hitGroupOpticalDepthObj{};
			hitGroupOpticalDepthObj.Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
			hitGroupOpticalDepthObj.pDesc = &hitGroupOpticalDepth;

			D3D12_HIT_GROUP_DESC hitGroupShadow{};
			hitGroupShadow.HitGroupExport = c_hitGroupShadowName;
			hitGroupShadow.Type = D3D12_HIT_GROUP_TYPE_TRIANGLES;
			hitGroupShadow.ClosestHitShaderImport = c_closestHitShadowShaderName;
			D3D12_STATE_SUBOBJECT hitGroupShadowObj{};
			hitGroupShadowObj.Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
			hitGroupShadowObj.pDesc = &hitGroupShadow;

			// Shader config
			// Defines the maximum sizes in bytes for the ray payload and attribute structure.
			D3D12_RAYTRACING_SHADER_CONFIG shaderConfig{};
			//shaderConfig.MaxPayloadSizeInBytes = 3 * sizeof(float) + sizeof(UINT);
			shaderConfig.MaxPayloadSizeInBytes = 4 * sizeof(float) + 3 * sizeof(float) + sizeof(UINT) + 4; // sizeof bool in HLSL is 4
			shaderConfig.MaxAttributeSizeInBytes = 2 * sizeof(float);
			D3D12_STATE_SUBOBJECT shaderConfigObj{};
			shaderConfigObj.Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG;
			shaderConfigObj.pDesc = &shaderConfig;

			D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION shaderAssociation{};
			const WCHAR* shaderExports[] = { c_raygenShaderName, c_opticalDepthRaygenShaderName };
			shaderAssociation.NumExports = 2;
			shaderAssociation.pExports = shaderExports;
			shaderAssociation.pSubobjectToAssociate = &shaderConfigObj;

			D3D12_STATE_SUBOBJECT shaderAssociationObj{};
			shaderAssociationObj.Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
			shaderAssociationObj.pDesc = &shaderAssociation;


			// Local root signature and shader association
			// This is a root signature that enables a shader to have unique arguments that come from shader tables.
			// Hit group and miss shaders in this sample are not using a local root signature and thus one is not associated with them.
			// Local root signature to be used in a ray gen shader.
			/*D3D12_LOCAL_ROOT_SIGNATURE localRootSignature{};
			localRootSignature.pLocalRootSignature = m_localRootSignature.Get();
			D3D12_STATE_SUBOBJECT localRootSignatureObj{};
			localRootSignatureObj.Type = D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE;
			localRootSignatureObj.pDesc = &localRootSignature.pLocalRootSignature;
			D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION rootSignatureAssociation{};
			rootSignatureAssociation.pSubobjectToAssociate = &localRootSignatureObj;
			rootSignatureAssociation.NumExports = 1;
			LPCWSTR rootsigexports[] = {c_hitGroupName};
			//rootsigexports[0] = c_raygenShaderName;
			rootSignatureAssociation.pExports = rootsigexports;
			D3D12_STATE_SUBOBJECT rootSignatureAssociationObj{};
			rootSignatureAssociationObj.Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
			rootSignatureAssociationObj.pDesc = &rootSignatureAssociation;*/

			// Global root signature
			// This is a root signature that is shared across all raytracing shaders invoked during a DispatchRays() call.
			D3D12_GLOBAL_ROOT_SIGNATURE globalRootSignature{};
			globalRootSignature.pGlobalRootSignature = m_globalRootSignature.Get();
			D3D12_STATE_SUBOBJECT globalRootSignatureObj{};
			globalRootSignatureObj.Type = D3D12_STATE_SUBOBJECT_TYPE_GLOBAL_ROOT_SIGNATURE;
			globalRootSignatureObj.pDesc = &globalRootSignature;

			// Pipeline config
			// Defines the maximum TraceRay() recursion depth.
			D3D12_RAYTRACING_PIPELINE_CONFIG pipelineConfig{};
			// PERFOMANCE TIP: Set max recursion depth as low as needed 
			// as drivers may apply optimization strategies for low recursion depths.
			pipelineConfig.MaxTraceRecursionDepth = MAX_TRACE_RECURSION_DEPTH; // primary rays only.
			D3D12_STATE_SUBOBJECT pipelineConfigObj{};
			pipelineConfigObj.Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG;
			pipelineConfigObj.pDesc = &pipelineConfig;

			std::vector<D3D12_STATE_SUBOBJECT> subobjects;
			subobjects.resize(8);
			subobjects[0] = libObj;
			subobjects[1] = hitGroupObj;
			subobjects[2] = hitGroupOpticalDepthObj;
			subobjects[3] = hitGroupShadowObj;
			subobjects[4] = shaderConfigObj;
			subobjects[5] = shaderAssociationObj;
			subobjects[6] = globalRootSignatureObj;
			subobjects[7] = pipelineConfigObj;
			const D3D12_STATE_OBJECT_DESC raytracingPipeline{ D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE, 8, subobjects.data() };

			//Microsoft::WRL::ComPtr<ID3D12Device5> dev = m_deviceResources->GetD3DDevice();
			DX::ThrowIfFailed(m_dxrDevice->CreateStateObject(&raytracingPipeline, IID_PPV_ARGS(&m_dxrStateObject)));
			m_dxrStateObject->SetName(L"Raytracing Pipeline");

			// from D3D12nBodyGravity in Microsoft Directx 12 examples
			// Describe and create the compute pipeline state object (PSO).
			D3D12_COMPUTE_PIPELINE_STATE_DESC computePsoDesc = {};
			computePsoDesc.pRootSignature = m_globalRootSignature.Get();
			computePsoDesc.CS = CD3DX12_SHADER_BYTECODE(&m_computeShader[0], m_computeShader.size());

			DX::ThrowIfFailed(m_dxrDevice->CreateComputePipelineState(&computePsoDesc, IID_PPV_ARGS(&m_computePipelineState)));
			NAME_D3D12_OBJECT(m_computePipelineState);

			D3D12_COMPUTE_PIPELINE_STATE_DESC computePsoDesc2 = {};
			computePsoDesc2.pRootSignature = m_globalRootSignature.Get();
			computePsoDesc2.CS = CD3DX12_SHADER_BYTECODE(&m_clearComputeShader[0], m_clearComputeShader.size());

			DX::ThrowIfFailed(m_dxrDevice->CreateComputePipelineState(&computePsoDesc2, IID_PPV_ARGS(&m_clearComputePipelineState)));
			NAME_D3D12_OBJECT(m_clearComputePipelineState);
		}
		});

	// Create and upload cube geometry resources to the GPU.
	auto createAssetsTask = createPipelineStateTask.then([this]() {
		//Microsoft::WRL::ComPtr<ID3D12Device5> d3dDevice = m_deviceResources->GetD3DDevice();

		D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc = {};
		// Allocate a heap for 3 descriptors:
		// 2 - vertex and index buffer SRVs
		// 1 - raytracing output texture SRV
		// 2 - textures SRV
		// 1 - optical depth output texture SRV
		// 1 - vec optical depth output texture SRV
		//descriptorHeapDesc.NumDescriptors = 4;
		//descriptorHeapDesc.NumDescriptors = 5;
		//descriptorHeapDesc.NumDescriptors = 6;
		descriptorHeapDesc.NumDescriptors = 7;
		descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		descriptorHeapDesc.NodeMask = 0;
		m_dxrDevice->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(&m_cbvHeap));
		NAME_D3D12_OBJECT(m_cbvHeap);

		m_cbvDescriptorSize = m_dxrDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		// Create a command list.
		DX::ThrowIfFailed(m_dxrDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_pipelineState.Get(), IID_PPV_ARGS(&m_commandList)));
		NAME_D3D12_OBJECT(m_commandList);

		// Cube vertices. Each vertex has a position and a color.
		//std::vector<VertexPositionColor> cubeVertices =
		m_cubeVertexBufferData =
		{
			{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f)}, // -x
			{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f)},
			{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f)},
			{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f)},

			{ XMFLOAT3(0.5f, -0.5f, -0.5f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f)}, // +x
			{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f)},
			{ XMFLOAT3(0.5f,  0.5f, -0.5f), XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f)},
			{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f)},

			{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, -1.0f, 0.0f)}, // -y
			{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, -1.0f, 0.0f)},
			{ XMFLOAT3(0.5f, -0.5f, -0.5f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f)},
			{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f)},

			{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f)}, // +y
			{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f)},
			{ XMFLOAT3(0.5f,  0.5f, -0.5f), XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f)},
			{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f)},

			{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f)}, // -z
			{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f)},
			{ XMFLOAT3(0.5f, -0.5f, -0.5f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f)},
			{ XMFLOAT3(0.5f,  0.5f, -0.5f), XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f)},

			{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f)}, // +z
			{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f)},
			{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f)},
			{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f)},
		};

		float sizeFactor = 1.0f;
		for (int i = 0; i < m_cubeVertexBufferData.size(); i++)
		{
			float x = m_cubeVertexBufferData[i].pos.x;
			float y = m_cubeVertexBufferData[i].pos.y;
			float z = m_cubeVertexBufferData[i].pos.z;
			m_cubeVertexBufferData[i].pos = XMFLOAT3(x * sizeFactor, y * sizeFactor, z * sizeFactor);
		}
		for (int i = 0; i < m_cubeVertexBufferData.size(); i++)
		{
			float x = m_cubeVertexBufferData[i].pos.x + 1.0f;
			float y = m_cubeVertexBufferData[i].pos.y - 1.0f;
			float z = m_cubeVertexBufferData[i].pos.z;
			m_cubeVertexBufferData[i].pos = XMFLOAT3(x, y, z);
		}

		//const UINT vertexBufferSize = sizeof(cubeVertices);

		/*std::vector<VertexPositionColor> sphereVertices;
		std::vector<unsigned short> sphereIndices;

		CreateSphere(sphereVertices, sphereIndices, 0.5f, 64, 64, 0.0f, XMFLOAT3(1.0f, 0.0f, 0.0f));
		CreateSphere(sphereVertices, sphereIndices, 0.5f, 64, 64, 1.0f, XMFLOAT3(0.0f, 0.0f, 1.0f));

		const UINT sphereVertexBufferSize = sizeof(sphereVertices[0]) * sphereVertices.size();
		const UINT sphereIndexBufferSize = sizeof(sphereIndices[0]) * sphereIndices.size();*/

		/*VertexPositionColor quadVertices[] =
		{
			{ XMFLOAT3(-0.5f, -0.5f, 0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f)}, // +z
			{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
			{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
			{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		};

		unsigned short quadIndices[] =
		{
			0, 1, 3,
			0, 3, 2,
		};*/

		//std::vector<VertexPositionColor> quadVertices;
		//std::vector<unsigned short> quadIndices;

		//CreateQuad(quadVertices, quadIndices, 0.5f, 0.5f, 0, 0, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f));

		GenerateParticles();

		m_quadVertexBufferData.resize(4);
		m_quadIndexBufferData.resize(6);
		CreateQuad(m_quadVertexBufferData, m_quadIndexBufferData, 10.0f, 10.0f, 0, 0, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f });
		for (auto &data : m_quadVertexBufferData)
		{
			XMMATRIX rotate = XMMatrixRotationX(XM_PIDIV2);
			data.pos.x -= 2.5f;
			data.pos.y -= 2.5f;
			XMVECTOR vPos = XMLoadFloat3(&data.pos);
			XMVECTOR vNorm = XMLoadFloat3(&data.normal);
			vPos = XMVector3Transform(vPos, rotate);
			vNorm = XMVector3Transform(vNorm, rotate);
			XMStoreFloat3(&data.pos, vPos);
			XMStoreFloat3(&data.normal, vNorm);
			data.pos.y -= 3.0f;
		}

		//m_vertexBufferData.insert(m_vertexBufferData.end(), cubeVertices.begin(), cubeVertices.end());
		m_vertexBufferData.insert(m_vertexBufferData.end(), m_quadVertexBufferData.begin(), m_quadVertexBufferData.end());
		m_vertexBufferData.insert(m_vertexBufferData.end(), m_cubeVertexBufferData.begin(), m_cubeVertexBufferData.end());

		//const UINT quadVertexBufferSize = sizeof(quadVertices[0]) * quadVertices.size();
		const UINT vertexBufferSize = sizeof(m_vertexBufferData[0]) * m_vertexBufferData.size();
		//const UINT quadIndexBufferSize = sizeof(m_indexBufferData[0]) * m_indexBufferData.size();

		auto uploadHeapPropertiesVertex = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
		//auto bufferDescVertex = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
		//auto bufferDescVertex = CD3DX12_RESOURCE_DESC::Buffer(sphereVertexBufferSize);
		auto bufferDescVertex = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
			&uploadHeapPropertiesVertex,
			D3D12_HEAP_FLAG_NONE,
			&bufferDescVertex,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_vertexBuffer)));
		void* pMappedDataVertex;
		(m_vertexBuffer)->Map(0, nullptr, &pMappedDataVertex);
		//memcpy(pMappedDataVertex, cubeVertices, vertexBufferSize);
		//memcpy(pMappedDataVertex, sphereVertices.data(), sphereVertexBufferSize);
		//memcpy(pMappedDataVertex, quadVertices, quadVertexBufferSize);
		//memcpy(pMappedDataVertex, quadVertices.data(), quadVertexBufferSize);
		memcpy(pMappedDataVertex, m_vertexBufferData.data(), vertexBufferSize);
		(m_vertexBuffer)->Unmap(0, nullptr);

		// Load mesh indices. Each trio of indices represents a triangle to be rendered on the screen.
		// For example: 0,2,1 means that the vertices with indexes 0, 2 and 1 from the vertex buffer compose the
		// first triangle of this mesh.

		//std::vector<unsigned short> cubeIndices =
		m_cubeIndexBufferData =
		{
			0, 2, 1, // -x
			1, 2, 3,

			4, 5, 6, // +x
			5, 7, 6,

			8, 9, 11, // -y
			8, 11, 10,

			12, 14, 15, // +y
			12, 15, 13,

			16, 18, 19, // -z
			16, 19, 17,

			20, 21, 23, // +z
			20, 23, 22,
		};

		//const UINT indexBufferSize = sizeof(cubeIndices);
		//m_indexBufferData.insert(m_indexBufferData.end(), cubeIndices.begin(), cubeIndices.end());
		m_indexBufferData.insert(m_indexBufferData.end(), m_quadIndexBufferData.begin(), m_quadIndexBufferData.end());
		m_indexBufferData.insert(m_indexBufferData.end(), m_cubeIndexBufferData.begin(), m_cubeIndexBufferData.end());

		const UINT indexBufferSize = sizeof(m_indexBufferData[0]) * m_indexBufferData.size();
		//const UINT quadIndexBufferSize = sizeof(m_indexBufferData[0]) * m_indexBufferData.size();

		auto uploadHeapPropertiesIndex = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
		auto bufferDescIndex = CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize);
		//auto bufferDescIndex = CD3DX12_RESOURCE_DESC::Buffer(sphereIndexBufferSize);
		//auto bufferDescIndex = CD3DX12_RESOURCE_DESC::Buffer(quadIndexBufferSize);
		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
			&uploadHeapPropertiesIndex,
			D3D12_HEAP_FLAG_NONE,
			&bufferDescIndex,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_indexBuffer)));
		void* pMappedDataIndex;
		(m_indexBuffer)->Map(0, nullptr, &pMappedDataIndex);
		//memcpy(pMappedDataIndex, cubeIndices, indexBufferSize);
		//memcpy(pMappedDataIndex, sphereIndices.data(), sphereIndexBufferSize);
		//memcpy(pMappedDataIndex, quadIndices, quadIndexBufferSize);
		//memcpy(pMappedDataIndex, m_indexBufferData.data(), quadIndexBufferSize);
		memcpy(pMappedDataIndex, m_indexBufferData.data(), indexBufferSize);
		(m_indexBuffer)->Unmap(0, nullptr);

		// SRV
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		//srvDesc.Buffer.NumElements = sizeof(cubeIndices)/4;
		//srvDesc.Buffer.NumElements = sphereIndices.size() / 2;
		//srvDesc.Buffer.NumElements = sizeof(quadIndices) / 4;
		srvDesc.Buffer.NumElements = m_indexBufferData.size() / 2;
		srvDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_RAW;
		srvDesc.Buffer.StructureByteStride = 0;
		auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
		UINT descriptorIndexToUse = m_descriptorsAllocated++;
		m_indexBufferResourceSRVCpuDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, descriptorIndexToUse, m_cbvDescriptorSize);
		UINT descriptorIndexIB = descriptorIndexToUse;
		m_dxrDevice->CreateShaderResourceView(m_indexBuffer.Get(), &srvDesc, m_indexBufferResourceSRVCpuDescriptor);
		m_indexBufferResourceSRVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndexIB, m_cbvDescriptorSize);

		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc2 = {};
		srvDesc2.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		srvDesc2.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		//srvDesc2.Buffer.NumElements = ARRAYSIZE(cubeVertices);
		//srvDesc2.Buffer.NumElements = sphereVertices.size();
		//srvDesc2.Buffer.NumElements = ARRAYSIZE(quadVertices);
		//srvDesc2.Buffer.NumElements = quadVertices.size();
		srvDesc2.Buffer.NumElements = m_vertexBufferData.size();
		srvDesc2.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc2.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		//srvDesc2.Buffer.StructureByteStride = sizeof(cubeVertices[0]);
		//srvDesc2.Buffer.StructureByteStride = sizeof(sphereVertices[0]);
		//srvDesc2.Buffer.StructureByteStride = sizeof(quadVertices[0]);
		srvDesc2.Buffer.StructureByteStride = sizeof(m_vertexBufferData[0]);
		auto descriptorHeapCpuBase2 = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
		UINT descriptorIndexToUse2 = m_descriptorsAllocated++;
		m_vertexBufferResourceSRVCpuDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase2, descriptorIndexToUse2, m_cbvDescriptorSize);
		UINT descriptorIndexVB = descriptorIndexToUse2;
		m_dxrDevice->CreateShaderResourceView(m_vertexBuffer.Get(), &srvDesc2, m_vertexBufferResourceSRVCpuDescriptor);
		m_vertexBufferResourceSRVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndexVB, m_cbvDescriptorSize);

		DX::ThrowIfFailed(descriptorIndexVB == descriptorIndexIB + 1 ? S_OK : E_FAIL);

		CreateTexture();

		// Repurposed from https://github.com/microsoft/DirectX-Graphics-Samples/blob/master/Samples/Desktop/D3D12Raytracing/src/D3D12RaytracingHelloWorld/D3D12RaytracingHelloWorld.cpp
		// Build acceleration structures needed for raytracing.
		{
			auto commandAllocator = m_deviceResources->GetCommandAllocator();

			m_bottomLevelAccelerationStructures.resize(m_nbrGeometries);
			std::vector<D3D12_RAYTRACING_GEOMETRY_DESC> geometryDescs(m_nbrGeometries);

			{
				D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc = {};
				geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
				geometryDesc.Triangles.IndexBuffer = m_indexBuffer->GetGPUVirtualAddress();
				geometryDesc.Triangles.IndexCount = m_particleIndexBufferData.size();
				geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_R16_UINT;
				geometryDesc.Triangles.Transform3x4 = 0;
				geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
				geometryDesc.Triangles.VertexCount = m_particleVertexBufferData.size();
				geometryDesc.Triangles.VertexBuffer.StartAddress = m_vertexBuffer->GetGPUVirtualAddress();
				geometryDesc.Triangles.VertexBuffer.StrideInBytes = sizeof(VertexPositionColor);
				//geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;
				geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NO_DUPLICATE_ANYHIT_INVOCATION;

				geometryDescs[0] = geometryDesc;
			}

			{
				D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc = {};
				geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
				geometryDesc.Triangles.IndexBuffer = m_indexBuffer->GetGPUVirtualAddress() + m_particleIndexBufferData.size() * sizeof(unsigned short);
				geometryDesc.Triangles.IndexCount = m_quadParticleIndexBufferData.size();
				geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_R16_UINT;
				geometryDesc.Triangles.Transform3x4 = 0;
				geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
				geometryDesc.Triangles.VertexCount = m_quadParticleVertexBufferData.size();
				geometryDesc.Triangles.VertexBuffer.StartAddress = m_vertexBuffer->GetGPUVirtualAddress() + m_particleVertexBufferData.size() * sizeof(VertexPositionColor);
				geometryDesc.Triangles.VertexBuffer.StrideInBytes = sizeof(VertexPositionColor);
				//geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;
				geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NO_DUPLICATE_ANYHIT_INVOCATION;

				geometryDescs[1] = geometryDesc;
			}

			
			{
				D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc = {};
				geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
				geometryDesc.Triangles.IndexBuffer = m_indexBuffer->GetGPUVirtualAddress() + m_particleIndexBufferData.size() * sizeof(unsigned short) + m_quadParticleIndexBufferData.size() * sizeof(unsigned short);
				geometryDesc.Triangles.IndexCount = m_quadIndexBufferData.size();
				geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_R16_UINT;
				geometryDesc.Triangles.Transform3x4 = 0;
				geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
				geometryDesc.Triangles.VertexCount = m_quadVertexBufferData.size();
				geometryDesc.Triangles.VertexBuffer.StartAddress = m_vertexBuffer->GetGPUVirtualAddress() + m_particleVertexBufferData.size() * sizeof(VertexPositionColor) + m_quadParticleVertexBufferData.size() * sizeof(VertexPositionColor);
				geometryDesc.Triangles.VertexBuffer.StrideInBytes = sizeof(VertexPositionColor);
				//geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;
				geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NO_DUPLICATE_ANYHIT_INVOCATION;
				geometryDescs[2] = geometryDesc;
			}
			{
				D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc2 = {};
				geometryDesc2.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
				geometryDesc2.Triangles.IndexBuffer = m_indexBuffer->GetGPUVirtualAddress() + m_particleIndexBufferData.size() * sizeof(unsigned short) + m_quadParticleIndexBufferData.size() * sizeof(unsigned short) + m_quadIndexBufferData.size() * sizeof(unsigned short);
				geometryDesc2.Triangles.IndexCount = m_cubeIndexBufferData.size();
				geometryDesc2.Triangles.IndexFormat = DXGI_FORMAT_R16_UINT;
				geometryDesc2.Triangles.Transform3x4 = 0;
				geometryDesc2.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
				geometryDesc2.Triangles.VertexCount = m_cubeVertexBufferData.size();
				geometryDesc2.Triangles.VertexBuffer.StartAddress = m_vertexBuffer->GetGPUVirtualAddress() + m_particleVertexBufferData.size() * sizeof(VertexPositionColor) + m_quadParticleVertexBufferData.size() * sizeof(VertexPositionColor) + m_quadVertexBufferData.size() * sizeof(VertexPositionColor);
				geometryDesc2.Triangles.VertexBuffer.StrideInBytes = sizeof(VertexPositionColor);
				geometryDesc2.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;
				geometryDescs[3] = geometryDesc2;
			}

			UINT nbrInstances = m_nbrParticles * 2 + (m_nbrGeometries - 2);
			// Get required sizes for an acceleration structure.
			D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS topLevelInputs = {};
			topLevelInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
			topLevelInputs.Flags = buildFlags;
			topLevelInputs.NumDescs = nbrInstances;
			topLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;

			D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO topLevelPrebuildInfo = {};
			m_dxrDevice->GetRaytracingAccelerationStructurePrebuildInfo(&topLevelInputs, &topLevelPrebuildInfo);
			DX::ThrowIfFailed(topLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0 ? S_OK : E_FAIL);

			std::vector<D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO> bottomLevelPrebuildInfos(m_nbrGeometries);
			std::vector<D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS> bottomLevelInputss(m_nbrGeometries);
			UINT64 maxBottomScratchSize = 0;
			for (int i = 0; i < m_nbrGeometries; i++)
			{
				D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO bottomLevelPrebuildInfo = {};
				D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS bottomLevelInputs = topLevelInputs;
				bottomLevelInputs.Flags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
				bottomLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
				bottomLevelInputs.pGeometryDescs = &geometryDescs[i];
				bottomLevelInputs.NumDescs = 1;
				m_dxrDevice->GetRaytracingAccelerationStructurePrebuildInfo(&bottomLevelInputs, &bottomLevelPrebuildInfo);
				DX::ThrowIfFailed(bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0 ? S_OK : E_FAIL);
				bottomLevelPrebuildInfos[i] = bottomLevelPrebuildInfo;
				bottomLevelInputss[i] = bottomLevelInputs;
				if (bottomLevelPrebuildInfo.ScratchDataSizeInBytes > maxBottomScratchSize)
				{
					maxBottomScratchSize = bottomLevelPrebuildInfo.ScratchDataSizeInBytes;
				}
			}

			CD3DX12_HEAP_PROPERTIES uploadHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
			UINT64 bufferSize = max(topLevelPrebuildInfo.ScratchDataSizeInBytes, maxBottomScratchSize);
			CD3DX12_RESOURCE_DESC bufferDesc = CD3DX12_RESOURCE_DESC::Buffer(bufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
			DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&bufferDesc,
				D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
				nullptr,
				IID_PPV_ARGS(&m_scratchResource))
			);
			m_scratchResource->SetName(L"ScratchResource");

			// Allocate resources for acceleration structures.
			// Acceleration structures can only be placed in resources that are created in the default heap (or custom heap equivalent). 
			// Default heap is OK since the application doesn?t need CPU read/write access to them. 
			// The resources that will contain acceleration structures must be created in the state D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE, 
			// and must have resource flag D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS. The ALLOW_UNORDERED_ACCESS requirement simply acknowledges both: 
			//  - the system will be doing this type of access in its implementation of acceleration structure builds behind the scenes.
			//  - from the app point of view, synchronization of writes/reads to acceleration structures is accomplished using UAV barriers.
			{
				D3D12_RESOURCE_STATES initialResourceState = D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE;

				CD3DX12_HEAP_PROPERTIES uploadHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
				std::vector<D3D12_RESOURCE_DESC> resDescs(m_nbrGeometries);
				for (int i = 0; i < m_nbrGeometries; i++)
				{
					UINT64 bufferSize = bottomLevelPrebuildInfos[i].ResultDataMaxSizeInBytes;
					CD3DX12_RESOURCE_DESC bufferDesc = CD3DX12_RESOURCE_DESC::Buffer(bufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
					DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
						&uploadHeapProperties,
						D3D12_HEAP_FLAG_NONE,
						&bufferDesc,
						initialResourceState,
						nullptr,
						IID_PPV_ARGS(&m_bottomLevelAccelerationStructures[i]))
					);
					m_bottomLevelAccelerationStructures[i]->SetName(L"BottomLevelAccelerationStructure");

					resDescs[i] = bufferDesc;
				}

				CD3DX12_HEAP_PROPERTIES uploadHeapProperties2 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
				UINT64 bufferSize2 = topLevelPrebuildInfo.ResultDataMaxSizeInBytes;
				CD3DX12_RESOURCE_DESC bufferDesc2 = CD3DX12_RESOURCE_DESC::Buffer(bufferSize2, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
				DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
					&uploadHeapProperties2,
					D3D12_HEAP_FLAG_NONE,
					&bufferDesc2,
					initialResourceState,
					nullptr,
					IID_PPV_ARGS(&m_topLevelAccelerationStructure))
				);
				m_topLevelAccelerationStructure->SetName(L"TopLevelAccelerationStructure");
			}

			std::vector<D3D12_RAYTRACING_INSTANCE_DESC> instanceDesc(nbrInstances);
			for (int i = 0; i < m_nbrParticles; i++)
			{
				D3D12_RAYTRACING_INSTANCE_DESC desc = {};
				desc.Transform[0][0] = desc.Transform[1][1] = desc.Transform[2][2] = 1;
				desc.InstanceID = i;
				desc.InstanceMask = 1;
				desc.AccelerationStructure = m_bottomLevelAccelerationStructures[0]->GetGPUVirtualAddress();
				instanceDesc[i] = desc;

				D3D12_RAYTRACING_INSTANCE_DESC desc2 = {};
				desc2.Transform[0][0] = desc2.Transform[1][1] = desc2.Transform[2][2] = 1;
				desc2.InstanceID = i + m_nbrParticles;
				desc2.InstanceMask = 2;
				desc2.AccelerationStructure = m_bottomLevelAccelerationStructures[1]->GetGPUVirtualAddress();
				instanceDesc[i + m_nbrParticles] = desc2;
			}

			D3D12_RAYTRACING_INSTANCE_DESC desc = {};
			desc.Transform[0][0] = desc.Transform[1][1] = desc.Transform[2][2] = 1;
			desc.InstanceMask = 3;
			desc.InstanceID = m_nbrParticles * 2;
			desc.AccelerationStructure = m_bottomLevelAccelerationStructures[2]->GetGPUVirtualAddress();
			instanceDesc[m_nbrParticles * 2] = desc;

			D3D12_RAYTRACING_INSTANCE_DESC desc2 = {};
			desc2.Transform[0][0] = desc2.Transform[1][1] = desc2.Transform[2][2] = 1;
			desc2.InstanceMask = 4;
			desc2.InstanceID = m_nbrParticles * 2 + 1;
			desc2.AccelerationStructure = m_bottomLevelAccelerationStructures[3]->GetGPUVirtualAddress();
			instanceDesc[m_nbrParticles * 2 + 1] = desc2;

			CD3DX12_HEAP_PROPERTIES uploadHeapProperties3 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
			UINT64 bufferSize3 = sizeof(instanceDesc[0]) * instanceDesc.size();
			CD3DX12_RESOURCE_DESC bufferDesc3 = CD3DX12_RESOURCE_DESC::Buffer(bufferSize3);
			DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
				&uploadHeapProperties3,
				D3D12_HEAP_FLAG_NONE,
				&bufferDesc3,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&m_instanceDescs))
			);
			m_instanceDescs->SetName(L"InstanceDescs");
			void* pMappedData;
			m_instanceDescs->Map(0, nullptr, &pMappedData);
			memcpy(pMappedData, instanceDesc.data(), sizeof(instanceDesc[0])*instanceDesc.size());
			m_instanceDescs->Unmap(0, nullptr);

			// Bottom Level Acceleration Structure desc
			std::vector<D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC>bottomLevelBuildDescs(m_nbrGeometries);
			for (int i = 0; i < m_nbrGeometries; i++)
			{
				D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC bottomLevelBuildDesc = {};
				{
					bottomLevelBuildDesc.Inputs = bottomLevelInputss[i];
					bottomLevelBuildDesc.ScratchAccelerationStructureData = m_scratchResource->GetGPUVirtualAddress();
					bottomLevelBuildDesc.DestAccelerationStructureData = m_bottomLevelAccelerationStructures[i]->GetGPUVirtualAddress();
				}
				bottomLevelBuildDescs[i] = bottomLevelBuildDesc;
			}
			// Top Level Acceleration Structure desc
			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC topLevelBuildDesc = {};
			{
				topLevelInputs.InstanceDescs = m_instanceDescs->GetGPUVirtualAddress();
				topLevelBuildDesc.Inputs = topLevelInputs;
				topLevelBuildDesc.DestAccelerationStructureData = m_topLevelAccelerationStructure->GetGPUVirtualAddress();
				topLevelBuildDesc.ScratchAccelerationStructureData = m_scratchResource->GetGPUVirtualAddress();
			}

			auto BuildAccelerationStructure = [&](auto* raytracingCommandList)
			{
				for (int i = 0; i < m_nbrGeometries; i++)
				{
					raytracingCommandList->BuildRaytracingAccelerationStructure(&bottomLevelBuildDescs[i], 0, nullptr);
					m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::UAV(m_bottomLevelAccelerationStructures[i].Get()));
				}
				raytracingCommandList->BuildRaytracingAccelerationStructure(&topLevelBuildDesc, 0, nullptr);
			};

			// Build acceleration structure.
			BuildAccelerationStructure(m_commandList.Get());

			// Close the command list and execute it to begin the vertex/index buffer copy into the GPU's default heap.
			DX::ThrowIfFailed(m_commandList->Close());
			ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
			m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

			// Wait for the command list to finish executing; the vertex/index buffers need to be uploaded to the GPU before the upload resources go out of scope.
			m_deviceResources->WaitForGpu();
		}

		//auto device = m_deviceResources->GetD3DDevice();
		//auto frameCount = m_deviceResources->GetBackBufferCount();

		// Create the constant buffer memory and map the CPU and GPU addresses
		const D3D12_HEAP_PROPERTIES uploadHeapPropertiesCB = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

		// Allocate one constant buffer per frame, since it gets updated every frame.
		size_t cbSize = DX::c_frameCount * sizeof(AlignedRayGenData);
		const D3D12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(cbSize);

		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
			&uploadHeapPropertiesCB,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_constantBuffer)));

		// Map the constant buffer and cache its heap pointers.
		// We don't unmap this until the app closes. Keeping buffer mapped for the lifetime of the resource is okay.
		CD3DX12_RANGE readRange(0, 0);        // We do not intend to read from this resource on the CPU.
		DX::ThrowIfFailed(m_constantBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_mappedConstantBuffer)));


		// Create the constant buffer memory and map the CPU and GPU addresses
		const D3D12_HEAP_PROPERTIES uploadHeapPropertiesCB2 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

		// Allocate one constant buffer per frame, since it gets updated every frame.
		size_t cbSize2 = DX::c_frameCount * sizeof(AlignedParticlePositionData);
		const D3D12_RESOURCE_DESC constantBufferDesc2 = CD3DX12_RESOURCE_DESC::Buffer(cbSize2);

		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
			&uploadHeapPropertiesCB2,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc2,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_particleConstantBuffer)));

		// Map the constant buffer and cache its heap pointers.
		// We don't unmap this until the app closes. Keeping buffer mapped for the lifetime of the resource is okay.
		CD3DX12_RANGE readRange2(0, 0);        // We do not intend to read from this resource on the CPU.
		DX::ThrowIfFailed(m_particleConstantBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_mappedParticleConstantBuffer)));

		// Create the constant buffer memory and map the CPU and GPU addresses
		const D3D12_HEAP_PROPERTIES uploadHeapPropertiesCB3 = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

		// Allocate one constant buffer per frame, since it gets updated every frame.
		size_t cbSize3 = DX::c_frameCount * sizeof(AlignedValuesData);
		const D3D12_RESOURCE_DESC constantBufferDesc3 = CD3DX12_RESOURCE_DESC::Buffer(cbSize3);

		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
			&uploadHeapPropertiesCB3,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc3,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_valuesConstantBuffer)));

		// Map the constant buffer and cache its heap pointers.
		// We don't unmap this until the app closes. Keeping buffer mapped for the lifetime of the resource is okay.
		CD3DX12_RANGE readRange3(0, 0);        // We do not intend to read from this resource on the CPU.
		DX::ThrowIfFailed(m_valuesConstantBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_mappedValuesConstantBuffer)));

		// Repurposed from https://github.com/acmarrs/IntroToDXR/blob/master/src/Graphics.cpp
		/**
		* Create the DXR shader table.
		*/
		{
			/*
			The Shader Table layout is as follows:
				Entry 0 - Ray Generation shader
				Entry 1 - Miss shader
				Entry 2 - Closest Hit shader
			All shader records in the Shader Table must have the same size, so shader record size will be based on the largest required entry.
			The ray generation program requires the largest entry:
				32 bytes - D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES
			  +  8 bytes - a CBV/SRV/UAV descriptor table pointer (64-bits)
			  = 40 bytes ->> aligns to 64 bytes
			The entry size must be aligned up to D3D12_RAYTRACING_SHADER_BINDING_TABLE_RECORD_BYTE_ALIGNMENT
			*/
			uint32_t shaderIdSize = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
			uint32_t shaderTableSize = 0;

			m_shaderTable.shaderTableRecordSize = shaderIdSize;
			m_shaderTable.shaderTableRecordSize += 8;		// CBV/SRV/UAV descriptor table
			m_shaderTable.shaderTableRecordSize = (m_shaderTable.shaderTableRecordSize + (D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT - 1)) & ~(D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT - 1);

			shaderTableSize = m_shaderTable.shaderTableRecordSize * 6;		// 6 shader records in the table
			shaderTableSize = (shaderTableSize + (D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT - 1)) & ~(D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT - 1);

			// Create the shader table buffer
			D3D12_HEAP_PROPERTIES heapDesc = {};
			heapDesc.Type = D3D12_HEAP_TYPE_UPLOAD;
			heapDesc.CreationNodeMask = 1;
			heapDesc.VisibleNodeMask = 1;
			D3D12_RESOURCE_DESC resourceDesc = {};
			resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			resourceDesc.Alignment = 0;
			resourceDesc.Height = 1;
			resourceDesc.DepthOrArraySize = 1;
			resourceDesc.MipLevels = 1;
			resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
			resourceDesc.SampleDesc.Count = 1;
			resourceDesc.SampleDesc.Quality = 0;
			resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			resourceDesc.Width = shaderTableSize;
			resourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
			DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
				&heapDesc,
				D3D12_HEAP_FLAG_NONE,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&m_shaderTable.table))
			);
			m_shaderTable.table->SetName(L"DXR Shader Table");

			// Map the buffer
			uint8_t* pData;
			DX::ThrowIfFailed(m_shaderTable.table->Map(0, nullptr, (void**)&pData));

			// Shader Record 0 - Ray Generation program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
			ComPtr<ID3D12StateObjectProperties> stateObjectProperties;
			DX::ThrowIfFailed(m_dxrStateObject.As(&stateObjectProperties));
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_raygenShaderName), shaderIdSize);

			// Set the root parameter data. Point to start of descriptor heap.
			//*reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(pData + shaderIdSize) = m_cbvHeap->GetGPUDescriptorHandleForHeapStart();

			// Shader Record 1 - Miss program (no local root arguments to set)
			pData += m_shaderTable.shaderTableRecordSize;
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_missShaderName), shaderIdSize);

			// Shader Record 2 - Miss Shadow program (no local root arguments to set)
			pData += m_shaderTable.shaderTableRecordSize;
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_missShadowShaderName), shaderIdSize);

			// Shader Record 3 - Closest Hit program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
			pData += m_shaderTable.shaderTableRecordSize;
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_hitGroupName), shaderIdSize);

			// Set the root parameter data. Point to start of descriptor heap.
			*reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(pData + shaderIdSize) = m_cbvHeap->GetGPUDescriptorHandleForHeapStart();

			// Shader Record 4 - Any Hit program (no local root arguments to set)
			pData += m_shaderTable.shaderTableRecordSize;
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_hitGroupName), shaderIdSize);

			// Shader Record 5 - Closest Hit Shadow program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
			pData += m_shaderTable.shaderTableRecordSize;
			memcpy(pData, stateObjectProperties->GetShaderIdentifier(c_hitGroupShadowName), shaderIdSize);

			m_shaderTable.table->Unmap(0, nullptr);


			uint32_t shaderTableSize2 = 0;

			m_opticalDepthShaderTable.shaderTableRecordSize = shaderIdSize;
			m_opticalDepthShaderTable.shaderTableRecordSize += 8;		// CBV/SRV/UAV descriptor table
			m_opticalDepthShaderTable.shaderTableRecordSize = (m_opticalDepthShaderTable.shaderTableRecordSize + (D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT - 1)) & ~(D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT - 1);

			shaderTableSize2 = m_opticalDepthShaderTable.shaderTableRecordSize * 4;		// 4 shader records in the table
			shaderTableSize2 = (shaderTableSize2 + (D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT - 1)) & ~(D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT - 1);

			// Create the shader table buffer
			D3D12_HEAP_PROPERTIES heapDesc2 = {};
			heapDesc2.Type = D3D12_HEAP_TYPE_UPLOAD;
			heapDesc2.CreationNodeMask = 1;
			heapDesc2.VisibleNodeMask = 1;
			D3D12_RESOURCE_DESC resourceDesc2 = {};
			resourceDesc2.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			resourceDesc2.Alignment = 0;
			resourceDesc2.Height = 1;
			resourceDesc2.DepthOrArraySize = 1;
			resourceDesc2.MipLevels = 1;
			resourceDesc2.Format = DXGI_FORMAT_UNKNOWN;
			resourceDesc2.SampleDesc.Count = 1;
			resourceDesc2.SampleDesc.Quality = 0;
			resourceDesc2.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			resourceDesc2.Width = shaderTableSize2;
			resourceDesc2.Flags = D3D12_RESOURCE_FLAG_NONE;
			DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
				&heapDesc2,
				D3D12_HEAP_FLAG_NONE,
				&resourceDesc2,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&m_opticalDepthShaderTable.table))
			);
			m_opticalDepthShaderTable.table->SetName(L"DXR Optical Depth Shader Table");

			// Map the buffer
			uint8_t* pData2;
			DX::ThrowIfFailed(m_opticalDepthShaderTable.table->Map(0, nullptr, (void**)&pData2));

			// Shader Record 0 - Ray Generation program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
			ComPtr<ID3D12StateObjectProperties> stateObjectProperties2;
			DX::ThrowIfFailed(m_dxrStateObject.As(&stateObjectProperties2));
			memcpy(pData2, stateObjectProperties2->GetShaderIdentifier(c_opticalDepthRaygenShaderName), shaderIdSize);

			// Set the root parameter data. Point to start of descriptor heap.
			//*reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(pData + shaderIdSize) = m_cbvHeap->GetGPUDescriptorHandleForHeapStart();

			// Shader Record 1 - MissOpticalDepth program (no local root arguments to set)
			pData2 += m_opticalDepthShaderTable.shaderTableRecordSize;
			memcpy(pData2, stateObjectProperties2->GetShaderIdentifier(c_missOpticalDepthShaderName), shaderIdSize);

			// Shader Record 2 - Closest Hit Optical Depth program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
			pData2 += m_opticalDepthShaderTable.shaderTableRecordSize;
			memcpy(pData2, stateObjectProperties2->GetShaderIdentifier(c_hitGroupOpticalDepthName), shaderIdSize);

			// Shader Record 3 - Any Hit Optical Depth program (no local root arguments to set)
			pData2 += m_opticalDepthShaderTable.shaderTableRecordSize;
			memcpy(pData2, stateObjectProperties2->GetShaderIdentifier(c_hitGroupOpticalDepthName), shaderIdSize);

			m_opticalDepthShaderTable.table->Unmap(0, nullptr);

			// Create 2D output texture for raytracing.
			{
				DXGI_FORMAT backBufferFormat = m_deviceResources->GetBackBufferFormat();

				// Create the output resource. The dimensions and format should match the swap-chain.
				CD3DX12_RESOURCE_DESC uavDesc = CD3DX12_RESOURCE_DESC::Tex2D(backBufferFormat, m_deviceResources->GetScreenViewport().Width, m_deviceResources->GetScreenViewport().Height, 1, 1, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

				CD3DX12_HEAP_PROPERTIES defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
				DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
					&defaultHeapProperties,
					D3D12_HEAP_FLAG_NONE,
					&uavDesc,
					D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
					nullptr,
					IID_PPV_ARGS(&m_raytracingOutput))
				);
				NAME_D3D12_OBJECT(m_raytracingOutput);

				D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
				auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
				//if (m_raytracingOutputResourceUAVDescriptorHeapIndex >= m_cbvHeap->GetDesc().NumDescriptors)
				//{
					m_raytracingOutputResourceUAVDescriptorHeapIndex = m_descriptorsAllocated++;
				//}
				uavDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, m_raytracingOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
				D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
				UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
				m_dxrDevice->CreateUnorderedAccessView(m_raytracingOutput.Get(), nullptr, &UAVDesc, uavDescriptorHandle);
				m_raytracingOutputResourceUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), m_raytracingOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
			}

			{
				DXGI_FORMAT backBufferFormat = m_deviceResources->GetBackBufferFormat();

				CD3DX12_RESOURCE_DESC uavDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(float) * m_nbrParticles, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

				CD3DX12_HEAP_PROPERTIES defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
				DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
					&defaultHeapProperties,
					D3D12_HEAP_FLAG_NONE,
					&uavDesc,
					D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
					nullptr,
					IID_PPV_ARGS(&m_opticalDepthOutput))
				);
				NAME_D3D12_OBJECT(m_opticalDepthOutput);

				D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
				auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
				//if (m_raytracingOutputResourceUAVDescriptorHeapIndex >= m_cbvHeap->GetDesc().NumDescriptors)
				//{
				m_opticalDepthOutputResourceUAVDescriptorHeapIndex = m_descriptorsAllocated++;
				//}
				uavDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, m_opticalDepthOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
				D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
				UAVDesc.Format = DXGI_FORMAT_UNKNOWN;
				UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
				UAVDesc.Buffer.NumElements = m_nbrParticles;
				UAVDesc.Buffer.StructureByteStride = sizeof(float);
				m_dxrDevice->CreateUnorderedAccessView(m_opticalDepthOutput.Get(), nullptr, &UAVDesc, uavDescriptorHandle);
				m_opticalDepthOutputResourceUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), m_opticalDepthOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
			}

			{
				DXGI_FORMAT backBufferFormat = m_deviceResources->GetBackBufferFormat();

				CD3DX12_RESOURCE_DESC uavDesc = CD3DX12_RESOURCE_DESC::Buffer(sizeof(float) * 2 * OPTICAL_DEPTH_CUTOFF_POINT * m_nbrParticles, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

				CD3DX12_HEAP_PROPERTIES defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
				DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(
					&defaultHeapProperties,
					D3D12_HEAP_FLAG_NONE,
					&uavDesc,
					D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
					nullptr,
					IID_PPV_ARGS(&m_vecOpticalDepthOutput))
				);
				NAME_D3D12_OBJECT(m_vecOpticalDepthOutput);

				D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
				auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
				//if (m_raytracingOutputResourceUAVDescriptorHeapIndex >= m_cbvHeap->GetDesc().NumDescriptors)
				//{
				m_vecOpticalDepthOutputResourceUAVDescriptorHeapIndex = m_descriptorsAllocated++;
				//}
				uavDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, m_vecOpticalDepthOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
				D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
				UAVDesc.Format = DXGI_FORMAT_UNKNOWN;
				UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
				UAVDesc.Buffer.NumElements = 2 * OPTICAL_DEPTH_CUTOFF_POINT * m_nbrParticles;
				UAVDesc.Buffer.StructureByteStride = sizeof(float);
				m_dxrDevice->CreateUnorderedAccessView(m_vecOpticalDepthOutput.Get(), nullptr, &UAVDesc, uavDescriptorHandle);
				m_vecOpticalDepthOutputResourceUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), m_vecOpticalDepthOutputResourceUAVDescriptorHeapIndex, m_cbvDescriptorSize);
			}
		}
		});

	createAssetsTask.then([this]() {
		m_loadingComplete = true;
		});
}

// Initializes view parameters when the window size changes.
void Sample3DSceneRenderer::CreateWindowSizeDependentResources()
{
	Size outputSize = m_deviceResources->GetOutputSize();
	float aspectRatio = outputSize.Width / outputSize.Height;
	float fovAngleY = 70.0f * XM_PI / 180.0f;

	D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
	m_scissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };

	// This is a simple example of change that can be made when the app is in
	// portrait or snapped view.
	if (aspectRatio < 1.0f)
	{
		fovAngleY *= 2.0f;
	}

	// Note that the OrientationTransform3D matrix is post-multiplied here
	// in order to correctly orient the scene to match the display orientation.
	// This post-multiplication step is required for any draw calls that are
	// made to the swap chain render target. For draw calls to other targets,
	// this transform should not be applied.

	// This sample makes use of a right-handed coordinate system using row-major matrices.
	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovRH(
		fovAngleY,
		aspectRatio,
		0.01f,
		100.0f
	);

	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);

	/*XMStoreFloat4x4(
		&m_constantBufferData.projection,
		XMMatrixTranspose(perspectiveMatrix * orientationMatrix)
	);*/

	// Eye is at (0,0.7,1.5), looking at point (0,-0.1,0) with the up-vector along the y-axis.
	//XMVECTOR eye = {0.0f, 0.7f, 1.5f, 1.0f};
	XMVECTOR eye = { 0.0f, 0.0f, 3.5f, 1.0f };
	//XMVECTOR at = { 0.0f, -0.1f, 0.0f, 1.0f };
	XMVECTOR at = { 0.0f, 0.0f, 0.0f, 1.0f };
	XMVECTOR up = { 0.0f, 1.0f, 0.0f, 0.0f };

	//XMVECTOR eye3 = { 0.0f, 0.7f, 1.5f };
#ifdef BASE_VIEW
	XMVECTOR eye3 = { 0.0f, 0.0f, 3.5f }; //base view
#endif
#ifdef VIEW1
	XMVECTOR eye3 = { 3.0f * 0.8f, 1.25f * 0.8f, 4.0f * 0.8f }; //view 1
#endif
#ifdef VIEW2
	XMVECTOR eye3 = { 0.5f, 0.0f, 3.5f }; //view 2
#endif
	//XMVECTOR at3 = { 0.0f, -0.1f, 0.0f };
	XMVECTOR at3 = { 0.0f, 0.0f, 0.0f };
	XMVECTOR up3 = { 0.0f, 1.0f, 0.0f };

	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();

	//XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(45.0f));
	//eye3 = XMVector3Transform(eye3, rotate);

	//XMStoreFloat4x4(&m_constantBufferData.view, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
	//XMStoreFloat3(&m_constantBufferData.wsCamPos, { 0.0f, 0.7f, 1.5f });
	m_constantBufferData[frameIndex].wsCamPos = eye3;

	XMMATRIX view = XMMatrixTranspose(XMMatrixLookAtLH(eye, at, up));
	//XMMATRIX proj = XMMatrixPerspectiveFovLH(fovAngleY, aspectRatio, 1.0f, 125.0f);
	XMMATRIX proj = XMMatrixTranspose(perspectiveMatrix * orientationMatrix);
	XMMATRIX viewProj = view * proj;

	m_constantBufferData[frameIndex].projectionToWorld = XMMatrixInverse(nullptr, viewProj);

	// Initialize the view and projection inverse matrices.
	/*XMVECTOR m_eye = {0.0f, 0.7f, -1.5f, 1.0f};
	XMVECTOR m_at = { 0.0f, -0.1f, 0.0f, 1.0f };
	XMVECTOR right = { 1.0f, 0.0f, 0.0f, 0.0f };

	XMVECTOR direction = XMVector4Normalize(m_at - m_eye);
	XMVECTOR m_up = XMVector3Normalize(XMVector3Cross(direction, right));

	// Rotate camera around Y axis.
	XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(45.0f));
	m_eye = XMVector3Transform(m_eye, rotate);
	m_up = XMVector3Transform(m_up, rotate);

	m_constantBufferData[frameIndex].wsCamPos = m_eye;
	float fovAngleY2 = 45.0f;
	XMMATRIX view = XMMatrixLookAtLH(m_eye, m_at, m_up);
	XMMATRIX proj = XMMatrixPerspectiveFovLH(XMConvertToRadians(fovAngleY2), aspectRatio, 1.0f, 125.0f);
	XMMATRIX viewProj = view * proj;

	m_constantBufferData[frameIndex].projectionToWorld = XMMatrixInverse(nullptr, viewProj);*/

	/*float a1 = 0.0f - 0.0f;
	float a2 = 0.7f - -0.1f;
	float a3 = 1.5f - 0.0f;
	XMVECTOR a = { a1, a2, a3};*/
	XMVECTOR a = eye3 - at3;
	XMVECTOR b = up3;

	XMVECTOR w = XMVector3Normalize(a);
	XMVECTOR bw = XMVector3Cross(b, w);
	XMVECTOR u = XMVector3Normalize(bw);
	XMVECTOR v = XMVector3Cross(w, u);

	m_constantBufferData[frameIndex].wsCamU = u;
	m_constantBufferData[frameIndex].wsCamV = v;
	m_constantBufferData[frameIndex].wsCamW = w;
	/*m_constantBufferData[frameIndex].wsCamU = {1.0f, 0.0f, 0.0f};
	m_constantBufferData[frameIndex].wsCamV = {0.0f, 1.0f, 0.0f};
	m_constantBufferData[frameIndex].wsCamW = {0.0f, 0.0f, 1.0f};*/

	XMFLOAT3 lightPosition;
	XMFLOAT3 lightAmbientColor;
	XMFLOAT3 lightDiffuseColor;

	lightPosition = XMFLOAT3(0.0f, 5.8f, 0.0f); //base view
	//lightPosition = XMFLOAT3(1.0f, 5.8f, 0.0f); //view 1
	//lightPosition = XMFLOAT3(-5.8f, 0.3f, 0.0f); //view 2
	m_constantBufferData[frameIndex].lightPos = XMLoadFloat3(&lightPosition);
	//m_constantBufferData[frameIndex].lightPos = eye3;

	lightAmbientColor = XMFLOAT3(0.5f, 0.5f, 0.5f);
	m_constantBufferData[frameIndex].lightAmbientColor = XMLoadFloat3(&lightAmbientColor);

	lightDiffuseColor = XMFLOAT3(1.0f, 0.4f, 0.0f);
	m_constantBufferData[frameIndex].lightDiffuseColor = XMLoadFloat3(&lightDiffuseColor);

	m_valuesConstantBufferData[frameIndex].currentParticleGroup = 0;
	m_valuesConstantBufferData[frameIndex].particleRot = {0.0f, 0.0f, 0.0f};
	m_valuesConstantBufferData[frameIndex].texFrame = 0;

	m_valuesConstantBufferData[frameIndex].basic_mode = 0;
	m_valuesConstantBufferData[frameIndex].simple_shadow_mode = 0;
	m_valuesConstantBufferData[frameIndex].full_optical_depth_mode = 1;

	// Apply the initial values to all frames' buffer instances.
	for (auto& constantBufferData : m_constantBufferData)
	{
		constantBufferData = m_constantBufferData[frameIndex];
	}

	for (int i = 0; i < m_nbrParticles / m_particleGroupAmount; i++)
	{
		m_particleConstantBufferData[frameIndex].positions[i] = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	}
	for (auto& particleConstantBufferData : m_particleConstantBufferData)
	{
		particleConstantBufferData = m_particleConstantBufferData[frameIndex];
	}

	for (auto& valuesConstantBufferData : m_valuesConstantBufferData)
	{
		valuesConstantBufferData = m_valuesConstantBufferData[frameIndex];
	}

	//m_cubeCB.albedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
}

// Called once per frame, rotates the cube and calculates the model and view matrices.
void Sample3DSceneRenderer::Update(DX::StepTimer const& timer)
{
	if (m_loadingComplete)
	{
		if (!m_tracking)
		{
			// Rotate the cube a small amount.
			m_angle = static_cast<float>(timer.GetElapsedSeconds()) * m_radiansPerSecond;
			m_elapsed_seconds = static_cast<float>(timer.GetTotalSeconds());
			CalculateFrameStats();
			//Rotate(m_angle);
			if (m_shouldLightRotate)
			{
				RotateLight(m_angle);
			}
			auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
			if (m_aKey)
			{
				//Rotate(m_angle);
				m_constantBufferData[frameIndex].wsCamPos += m_constantBufferData[frameIndex].wsCamU * 0.01f;
			}
			else if (m_dKey)
			{
				//Rotate(-m_angle);
				m_constantBufferData[frameIndex].wsCamPos -= m_constantBufferData[frameIndex].wsCamU * 0.01f;
			}
			if (m_wKey)
			{
				//m_constantBufferData[frameIndex].wsCamPos.m128_f32[2] -= 0.1f;
				m_constantBufferData[frameIndex].wsCamPos -= m_constantBufferData[frameIndex].wsCamW * 0.01f;
			}
			else if (m_sKey)
			{
				//m_constantBufferData[frameIndex].wsCamPos.m128_f32[2] += 0.1f;
				m_constantBufferData[frameIndex].wsCamPos += m_constantBufferData[frameIndex].wsCamW * 0.01f;
			}
			if (m_qKey)
			{
				m_constantBufferData[frameIndex].wsCamPos += m_constantBufferData[frameIndex].wsCamV * 0.01f;
			}
			else if (m_eKey)
			{
				m_constantBufferData[frameIndex].wsCamPos -= m_constantBufferData[frameIndex].wsCamV * 0.01f;
			}

			if (m_upKey)
			{
				RotateCamX(m_angle * 1.5f);
			}
			else if (m_downKey)
			{
				RotateCamX(-m_angle * 1.5f);
			}
			if (m_leftKey)
			{
				RotateCamY(-m_angle * 1.5f);
			}
			else if (m_rightKey)
			{
				RotateCamY(m_angle * 1.5f);
			}

			for (auto& constantBufferData : m_constantBufferData)
			{
				constantBufferData = m_constantBufferData[frameIndex];
			}
			for (auto& constantBufferData : m_constantBufferData)
			{
				constantBufferData = m_constantBufferData[frameIndex];
			}
		}

		// Update the constant buffer resource.
		//auto destination = m_mappedConstantBuffer + (m_deviceResources->GetCurrentFrameIndex() * c_alignedConstantBufferSize);
		//memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));
	}
}

// Saves the current state of the renderer.
void Sample3DSceneRenderer::SaveState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;

	if (state->HasKey(AngleKey))
	{
		state->Remove(AngleKey);
	}
	if (state->HasKey(TrackingKey))
	{
		state->Remove(TrackingKey);
	}

	state->Insert(AngleKey, PropertyValue::CreateSingle(m_angle));
	state->Insert(TrackingKey, PropertyValue::CreateBoolean(m_tracking));
}

// Restores the previous state of the renderer.
void Sample3DSceneRenderer::LoadState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;
	if (state->HasKey(AngleKey))
	{
		m_angle = safe_cast<IPropertyValue^>(state->Lookup(AngleKey))->GetSingle();
		state->Remove(AngleKey);
	}
	if (state->HasKey(TrackingKey))
	{
		m_tracking = safe_cast<IPropertyValue^>(state->Lookup(TrackingKey))->GetBoolean();
		state->Remove(TrackingKey);
	}
}

// Rotate the 3D cube model a set amount of radians.
void Sample3DSceneRenderer::Rotate(float radians)
{
	// Prepare to pass the updated model matrix to the shader.
	//XMStoreFloat4x4(&m_constantBufferData.model, XMMatrixTranspose(XMMatrixRotationY(radians)));

	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
	UINT lastIndex;
	if (frameIndex == 0) {
		lastIndex = DX::c_frameCount - 1;
	}
	else {
		lastIndex = frameIndex - 1;
	}

	XMMATRIX rotate = XMMatrixRotationY(radians);
	XMMATRIX rotate2 = XMMatrixRotationY(radians / 2.0f);
	m_constantBufferData[frameIndex].wsCamPos = XMVector3Transform(m_constantBufferData[lastIndex].wsCamPos, rotate);
	m_constantBufferData[frameIndex].wsCamU = XMVector3Transform(m_constantBufferData[lastIndex].wsCamU, rotate);
	m_constantBufferData[frameIndex].wsCamV = XMVector3Transform(m_constantBufferData[lastIndex].wsCamV, rotate);
	m_constantBufferData[frameIndex].wsCamW = XMVector3Transform(m_constantBufferData[lastIndex].wsCamW, rotate);
	//m_constantBufferData[frameIndex].lightPos = XMVector3Transform(m_constantBufferData[lastIndex].lightPos, rotate2);
}

void Sample3DSceneRenderer::RotateLight(float radians)
{
	// Prepare to pass the updated model matrix to the shader.
	//XMStoreFloat4x4(&m_constantBufferData.model, XMMatrixTranspose(XMMatrixRotationY(radians)));

	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
	UINT lastIndex;
	if (frameIndex == 0) {
		lastIndex = DX::c_frameCount - 1;
	}
	else {
		lastIndex = frameIndex - 1;
	}

	XMMATRIX rotate = XMMatrixRotationZ(radians / 2.0f);
	m_constantBufferData[frameIndex].lightPos = XMVector3Transform(m_constantBufferData[lastIndex].lightPos, rotate);
}

void Sample3DSceneRenderer::RotateCamX(float radians)
{
	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
	UINT lastIndex;
	if (frameIndex == 0) {
		lastIndex = DX::c_frameCount - 1;
	}
	else {
		lastIndex = frameIndex - 1;
	}

	//XMMATRIX rotate = XMMatrixRotationX(radians);
	XMMATRIX rotate = XMMatrixRotationAxis(m_constantBufferData[frameIndex].wsCamU, radians);
	m_constantBufferData[frameIndex].wsCamU = XMVector3Transform(m_constantBufferData[lastIndex].wsCamU, rotate);
	m_constantBufferData[frameIndex].wsCamV = XMVector3Transform(m_constantBufferData[lastIndex].wsCamV, rotate);
	m_constantBufferData[frameIndex].wsCamW = XMVector3Transform(m_constantBufferData[lastIndex].wsCamW, rotate);
}

void Sample3DSceneRenderer::RotateCamY(float radians)
{
	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
	UINT lastIndex;
	if (frameIndex == 0) {
		lastIndex = DX::c_frameCount - 1;
	}
	else {
		lastIndex = frameIndex - 1;
	}

	//XMMATRIX rotate = XMMatrixRotationY(radians);
	XMMATRIX rotate = XMMatrixRotationAxis(m_constantBufferData[frameIndex].wsCamV, radians);
	m_constantBufferData[frameIndex].wsCamU = XMVector3Transform(m_constantBufferData[lastIndex].wsCamU, rotate);
	m_constantBufferData[frameIndex].wsCamV = XMVector3Transform(m_constantBufferData[lastIndex].wsCamV, rotate);
	m_constantBufferData[frameIndex].wsCamW = XMVector3Transform(m_constantBufferData[lastIndex].wsCamW, rotate);
}

void Sample3DSceneRenderer::StartTracking()
{
	m_tracking = true;
}

// When tracking, the 3D cube can be rotated around its Y axis by tracking pointer position relative to the output screen width.
void Sample3DSceneRenderer::TrackingUpdate(float positionX)
{
	if (m_tracking)
	{
		float radians = XM_2PI * 2.0f * positionX / m_deviceResources->GetOutputSize().Width;
		Rotate(radians);
	}
}

void Sample3DSceneRenderer::StopTracking()
{
	m_tracking = false;
}

// Renders one frame using the vertex and pixel shaders.
bool Sample3DSceneRenderer::Render()
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete)
	{
		return false;
	}
	m_gpuTimer.BeginFrame(m_commandList.Get());

	DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());
	DX::ThrowIfFailed(m_commandList->Reset(m_deviceResources->GetCommandAllocator(), nullptr));
	m_gpuTimer.Start(m_commandList.Get(), 4);
	//if (beforeState != D3D12_RESOURCE_STATE_RENDER_TARGET)
	{
		// Transition the render target into the correct state to allow for drawing into it.
		D3D12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_deviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_commandList->ResourceBarrier(1, &barrier);
	}

	//auto commandList = m_deviceResources->GetCommandList();

	auto DispatchOpticalDepthRays = [&](auto* commandList, auto* stateObject, auto* dispatchDesc)
	{
		// Since each shader table has only one shader record, the stride is same as the size.
		dispatchDesc->RayGenerationShaderRecord.StartAddress = m_opticalDepthShaderTable.table->GetGPUVirtualAddress();
		dispatchDesc->RayGenerationShaderRecord.SizeInBytes = m_opticalDepthShaderTable.shaderTableRecordSize;
		dispatchDesc->MissShaderTable.StartAddress = m_opticalDepthShaderTable.table->GetGPUVirtualAddress() + m_opticalDepthShaderTable.shaderTableRecordSize;
		dispatchDesc->MissShaderTable.SizeInBytes = m_opticalDepthShaderTable.shaderTableRecordSize;
		dispatchDesc->MissShaderTable.StrideInBytes = m_opticalDepthShaderTable.shaderTableRecordSize;
		dispatchDesc->HitGroupTable.StartAddress = m_opticalDepthShaderTable.table->GetGPUVirtualAddress() + (m_opticalDepthShaderTable.shaderTableRecordSize * 2);
		dispatchDesc->HitGroupTable.SizeInBytes = m_opticalDepthShaderTable.shaderTableRecordSize * 2;
		dispatchDesc->HitGroupTable.StrideInBytes = m_opticalDepthShaderTable.shaderTableRecordSize;
		dispatchDesc->Width = m_nbrParticles / m_particleGroupAmount;
		dispatchDesc->Height = 1;
		dispatchDesc->Depth = 1;
		commandList->SetPipelineState1(stateObject);

		m_gpuTimer.Start(commandList, 0);
		commandList->DispatchRays(dispatchDesc);
		m_gpuTimer.Stop(commandList, 0);
	};

	auto DispatchRays = [&](auto* commandList, auto* stateObject, auto* dispatchDesc)
	{
		// Since each shader table has only one shader record, the stride is same as the size.
		dispatchDesc->RayGenerationShaderRecord.StartAddress = m_shaderTable.table->GetGPUVirtualAddress();
		dispatchDesc->RayGenerationShaderRecord.SizeInBytes = m_shaderTable.shaderTableRecordSize;
		dispatchDesc->MissShaderTable.StartAddress = m_shaderTable.table->GetGPUVirtualAddress() + m_shaderTable.shaderTableRecordSize;
		dispatchDesc->MissShaderTable.SizeInBytes = m_shaderTable.shaderTableRecordSize * 2;
		dispatchDesc->MissShaderTable.StrideInBytes = m_shaderTable.shaderTableRecordSize;
		dispatchDesc->HitGroupTable.StartAddress = m_shaderTable.table->GetGPUVirtualAddress() + (m_shaderTable.shaderTableRecordSize * 3);
		dispatchDesc->HitGroupTable.SizeInBytes = m_shaderTable.shaderTableRecordSize * 3;
		dispatchDesc->HitGroupTable.StrideInBytes = m_shaderTable.shaderTableRecordSize;
		dispatchDesc->Width = m_deviceResources->GetScreenViewport().Width;
		dispatchDesc->Height = m_deviceResources->GetScreenViewport().Height;
		dispatchDesc->Depth = 1;
		commandList->SetPipelineState1(stateObject);

		m_gpuTimer.Start(commandList, 1);
		commandList->DispatchRays(dispatchDesc);
		m_gpuTimer.Stop(commandList, 1);
	};

	m_commandList->SetComputeRootSignature(m_globalRootSignature.Get());

	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();

	// Copy the updated scene constant buffer to GPU.
	memcpy(&m_mappedConstantBuffer[frameIndex].data, &m_constantBufferData[frameIndex], sizeof(m_constantBufferData[frameIndex]));
	auto cbGpuAddress = m_constantBuffer->GetGPUVirtualAddress() + (frameIndex * sizeof(m_mappedConstantBuffer[0]));
	m_commandList->SetComputeRootConstantBufferView(2, cbGpuAddress);

	memcpy(&m_mappedParticleConstantBuffer[frameIndex].data, &m_particleConstantBufferData[frameIndex], sizeof(m_particleConstantBufferData[frameIndex]));
	auto cbGpuAddress2 = m_particleConstantBuffer->GetGPUVirtualAddress() + (frameIndex * sizeof(m_mappedParticleConstantBuffer[0]));
	m_commandList->SetComputeRootConstantBufferView(4, cbGpuAddress2);

	memcpy(&m_mappedValuesConstantBuffer[frameIndex].data, &m_valuesConstantBufferData[frameIndex], sizeof(m_valuesConstantBufferData[frameIndex]));
	auto cbGpuAddress3 = m_valuesConstantBuffer->GetGPUVirtualAddress() + (frameIndex * sizeof(m_mappedValuesConstantBuffer[0]));
	m_commandList->SetComputeRootConstantBufferView(5, cbGpuAddress3);

	UpdateTopLevelAS();

	// Bind the heaps, acceleration structure and dispatch rays.    
	D3D12_DISPATCH_RAYS_DESC dispatchDesc = {};
	m_commandList->SetDescriptorHeaps(1, m_cbvHeap.GetAddressOf());
	m_commandList->SetComputeRootDescriptorTable(0, m_raytracingOutputResourceUAVGpuDescriptor);
	m_commandList->SetComputeRootDescriptorTable(3, m_indexBufferResourceSRVGpuDescriptor);
	//m_commandList->SetComputeRootDescriptorTable(4, m_textureResourceSRVGpuDescriptor);
	m_commandList->SetComputeRootShaderResourceView(1, m_topLevelAccelerationStructure->GetGPUVirtualAddress());
	DispatchOpticalDepthRays(m_commandList.Get(), m_dxrStateObject.Get(), &dispatchDesc);

	m_commandList->SetPipelineState(m_computePipelineState.Get());
	m_gpuTimer.Start(m_commandList.Get(), 2);
	m_commandList->Dispatch(std::ceil(m_nbrParticles / m_particleGroupAmount / static_cast<float>(BLOCKSIZE)), 1, 1);
	m_gpuTimer.Stop(m_commandList.Get(), 2);

	m_commandList->SetPipelineState(m_clearComputePipelineState.Get());
	m_gpuTimer.Start(m_commandList.Get(), 3);
	m_commandList->Dispatch(std::ceil((m_nbrParticles * 2 * OPTICAL_DEPTH_CUTOFF_POINT) / static_cast<float>(BLOCKSIZE)), 1, 1);
	m_gpuTimer.Stop(m_commandList.Get(), 3);

	DispatchRays(m_commandList.Get(), m_dxrStateObject.Get(), &dispatchDesc);

	//auto commandList = m_deviceResources->GetCommandList();
	auto renderTarget = m_deviceResources->GetRenderTarget();

	D3D12_RESOURCE_BARRIER preCopyBarriers[2];
	preCopyBarriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(renderTarget, D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_COPY_DEST);
	preCopyBarriers[1] = CD3DX12_RESOURCE_BARRIER::Transition(m_raytracingOutput.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_SOURCE);
	m_commandList->ResourceBarrier(ARRAYSIZE(preCopyBarriers), preCopyBarriers);

	m_commandList->CopyResource(renderTarget, m_raytracingOutput.Get());

	D3D12_RESOURCE_BARRIER postCopyBarriers[2];
	postCopyBarriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(renderTarget, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PRESENT);
	postCopyBarriers[1] = CD3DX12_RESOURCE_BARRIER::Transition(m_raytracingOutput.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

	m_commandList->ResourceBarrier(ARRAYSIZE(postCopyBarriers), postCopyBarriers);

	m_gpuTimer.Stop(m_commandList.Get(), 4);
	m_gpuTimer.EndFrame(m_commandList.Get());

	DX::ThrowIfFailed(m_commandList->Close());
	ID3D12CommandList* commandLists[] = { m_commandList.Get() };
	m_deviceResources->GetCommandQueue()->ExecuteCommandLists(ARRAYSIZE(commandLists), commandLists);

	return true;
}

// Inspired by: https://developer.nvidia.com/rtx/raytracing/dxr/dx12-raytracing-tutorial/extra/dxr_tutorial_extra_refit
void Sample3DSceneRenderer::UpdateTopLevelAS()
{
	float radians = m_elapsed_seconds * m_radiansPerSecond;
	XMMATRIX rotate = XMMatrixRotationY(radians);
	XMFLOAT4 rVecs[4]{};
	XMStoreFloat4(&rVecs[0], rotate.r[0]);
	XMStoreFloat4(&rVecs[1], rotate.r[1]);
	XMStoreFloat4(&rVecs[2], rotate.r[2]);
	XMStoreFloat4(&rVecs[3], rotate.r[3]);

	static double elapsedTime2 = 0.0f;

	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
	XMFLOAT3 u;
	XMFLOAT3 v;
	XMFLOAT3 w;
	XMStoreFloat3(&u, m_constantBufferData[frameIndex].wsCamU);
	XMStoreFloat3(&v, m_constantBufferData[frameIndex].wsCamV);
	XMStoreFloat3(&w, m_constantBufferData[frameIndex].wsCamW);
	std::vector<float> vec(16);
	vec[0] = u.x;
	vec[1] = u.y;
	vec[2] = u.z;
	vec[3] = 0;
	vec[4] = v.x;
	vec[5] = v.y;
	vec[6] = v.z;
	vec[7] = 0;
	vec[8] = w.x;
	vec[9] = w.y;
	vec[10] = w.z;
	vec[11] = 0;
	vec[12] = 0;
	vec[13] = 0;
	vec[14] = 0;
	vec[15] = 1;
	XMMATRIX mat(vec.data());
	XMMATRIX matInv = XMMatrixInverse(nullptr, mat);
	XMVECTOR vecInv[4]{};
	vecInv[0] = matInv.r[0];
	vecInv[1] = matInv.r[1];
	vecInv[2] = matInv.r[2];
	vecInv[3] = matInv.r[3];
	XMFLOAT3 uInv;
	XMFLOAT3 vInv;
	XMFLOAT3 wInv;
	XMStoreFloat3(&uInv, vecInv[0]);
	XMStoreFloat3(&vInv, vecInv[1]);
	XMStoreFloat3(&wInv, vecInv[2]);

	XMFLOAT3 x = { 1.0f, 0.0f, 0.0f };
	XMFLOAT3 y = { 0.0f, 1.0f, 0.0f };
	XMFLOAT3 z = { 0.0f, 0.0f, 1.0f };

	UINT nbrDescs = m_nbrParticles * 2 + (m_nbrGeometries - 2);
	std::vector<D3D12_RAYTRACING_INSTANCE_DESC> descs(nbrDescs);

	auto idx = 0;
	for (int i = 0; i < m_nbrParticles; i++)
	{
		/*float maxTime = m_motionVectors[i].y / m_motionVelocities[i];
		float time = m_elapsed_seconds;
		while (time > maxTime)
		{
			time -= maxTime;
		}

		std::vector<float> vec1(16);
		vec1[0] = 1;
		vec1[1] = 0;
		vec1[2] = 0;
		vec1[3] = m_offsets[i].x + m_motionVectors[i].x * time * m_motionVelocities[i];
		vec1[4] = 0;
		vec1[5] = 1;
		vec1[6] = 0;
		vec1[7] = m_offsets[i].y + m_motionVectors[i].y * time * m_motionVelocities[i];
		vec1[8] = 0;
		vec1[9] = 0;
		vec1[10] = 1;
		vec1[11] = m_offsets[i].z + m_motionVectors[i].z * time * m_motionVelocities[i];
		vec1[12] = 0;
		vec1[13] = 0;
		vec1[14] = 0;
		vec1[15] = 1;
		XMMATRIX mat1(vec1.data());

		std::vector<float> vec2(16);
		vec2[0] = m_scalingSpeeds[i] * time + 1;
		vec2[1] = 0;
		vec2[2] = 0;
		vec2[3] = 0;
		vec2[4] = 0;
		vec2[5] = m_scalingSpeeds[i] * time + 1;
		vec2[6] = 0;
		vec2[7] = 0;
		vec2[8] = 0;
		vec2[9] = 0;
		vec2[10] = m_scalingSpeeds[i] * time + 1;
		vec2[11] = 0;
		vec2[12] = 0;
		vec2[13] = 0;
		vec2[14] = 0;
		vec2[15] = 1;
		XMMATRIX mat2(vec2.data());

		//XMMATRIX mat3 = XMMatrixRotationZ(m_rotSpeeds[i] * m_elapsed_seconds);
		XMMATRIX mat3 = XMMatrixRotationAxis(m_constantBufferData[frameIndex].wsCamW, m_rotSpeeds[i] * time);

		//XMMATRIX sphereMat = XMMatrixMultiply(mat2, mat1);
		XMMATRIX sphereMat = XMMatrixMultiply(mat1, mat2);*/
		//XMMATRIX sphereMat = mat1;

		//XMVECTOR offset = XMVectorSet(m_offsets[i].x, m_offsets[i].y, m_offsets[i].z, 1.0f);

		D3D12_RAYTRACING_INSTANCE_DESC desc = {};
		desc.Transform[0][0] = desc.Transform[1][1] = desc.Transform[2][2] = 1;

#ifdef PARTICLE_MOVEMENT
		desc.Transform[0][3] = m_offsets[i].x + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].x * m_motionLength;
		desc.Transform[1][3] = m_offsets[i].y + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].y * m_motionLength;
		desc.Transform[2][3] = m_offsets[i].z + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].z * m_motionLength;
#else
		desc.Transform[0][3] = m_offsets[i].x;
		desc.Transform[1][3] = m_offsets[i].y;
		desc.Transform[2][3] = m_offsets[i].z;
#endif

		/*desc.Transform[0][0] = sphereMat.r[0].m128_f32[0];
		desc.Transform[0][1] = sphereMat.r[0].m128_f32[1];
		desc.Transform[0][2] = sphereMat.r[0].m128_f32[2];
		desc.Transform[0][3] = sphereMat.r[0].m128_f32[3];

		desc.Transform[1][0] = sphereMat.r[1].m128_f32[0];
		desc.Transform[1][1] = sphereMat.r[1].m128_f32[1];
		desc.Transform[1][2] = sphereMat.r[1].m128_f32[2];
		desc.Transform[1][3] = sphereMat.r[1].m128_f32[3];

		desc.Transform[2][0] = sphereMat.r[2].m128_f32[0];
		desc.Transform[2][1] = sphereMat.r[2].m128_f32[1];
		desc.Transform[2][2] = sphereMat.r[2].m128_f32[2];
		desc.Transform[2][3] = sphereMat.r[2].m128_f32[3];*/

		/*if (i % m_particleGroupAmount == m_currentParticleGroup)
		{
			m_particleConstantBufferData[frameIndex].positions[idx++] = XMFLOAT4(desc.Transform[0][3], desc.Transform[1][3], desc.Transform[2][3], i);
		}*/
		m_particleConstantBufferData[frameIndex].positions[i] = XMFLOAT4(desc.Transform[0][3], desc.Transform[1][3], desc.Transform[2][3], i);

		desc.InstanceMask = 1;
		desc.InstanceID = i;
		desc.AccelerationStructure = m_bottomLevelAccelerationStructures[0]->GetGPUVirtualAddress();
		descs[i] = desc;

		D3D12_RAYTRACING_INSTANCE_DESC desc2 = {};
		/*desc2.Transform[0][0] = desc2.Transform[1][1] = desc2.Transform[2][2] = 1;
#ifdef PARTICLE_MOVEMENT
		desc2.Transform[0][3] = m_offsets[i].x + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].x * m_motionLength;
		desc2.Transform[1][3] = m_offsets[i].y + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].y * m_motionLength;
		desc2.Transform[2][3] = m_offsets[i].z + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].z * m_motionLength;
#else
		desc2.Transform[0][3] = m_offsets[i].x - 1.5f;
		desc2.Transform[1][3] = m_offsets[i].y - 1.5f;
		desc2.Transform[2][3] = m_offsets[i].z;
#endif*/

#ifdef PARTICLE_MOVEMENT
		float offsetX = m_offsets[i].x + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].x * m_motionLength;
		float offsetY = m_offsets[i].y + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].y * m_motionLength;
		float offsetZ = m_offsets[i].z + std::cos(m_elapsed_seconds * m_motionSpeed) * m_motionVectors[i].z * m_motionLength;
#else
		float offsetX = m_offsets[i].x - 1.5f;
		float offsetY = m_offsets[i].y - 1.5f;
		float offsetZ = m_offsets[i].z;
#endif

		std::vector<float> moveVec(16);
		moveVec[0] = 1;
		moveVec[1] = 0;
		moveVec[2] = 0;
		moveVec[3] = offsetX;
		moveVec[4] = 0;
		moveVec[5] = 1;
		moveVec[6] = 0;
		moveVec[7] = offsetY;
		moveVec[8] = 0;
		moveVec[9] = 0;
		moveVec[10] = 1;
		moveVec[11] = offsetZ;
		moveVec[12] = 0;
		moveVec[13] = 0;
		moveVec[14] = 0;
		moveVec[15] = 1;
		XMMATRIX moveMat(moveVec.data());

		std::vector<float> rotVec(16);
		rotVec[0] = uInv.x;
		rotVec[1] = uInv.y;
		rotVec[2] = uInv.z;
		rotVec[3] = 0;
		rotVec[4] = vInv.x;
		rotVec[5] = vInv.y;
		rotVec[6] = vInv.z;
		rotVec[7] = 0;
		rotVec[8] = wInv.x;
		rotVec[9] = wInv.y;
		rotVec[10] = wInv.z;
		rotVec[11] = 0;
		rotVec[12] = 0;
		rotVec[13] = 0;
		rotVec[14] = 0;
		rotVec[15] = 1;
		XMMATRIX rotMat(rotVec.data());

		XMMATRIX transformMat = XMMatrixMultiply(moveMat, rotMat);

		//XMMATRIX quadMat = XMMatrixMultiply(XMMatrixMultiply(XMMatrixMultiply(mat2, rotMat), mat3), moveMat);
		/*
		XMMATRIX quadMat = XMMatrixMultiply(XMMatrixMultiply(XMMatrixMultiply(mat1, mat3), rotMat), mat2);
		*/
		//XMMATRIX quadMat = mat1;

		desc2.Transform[0][0] = transformMat.r[0].m128_f32[0];
		desc2.Transform[0][1] = transformMat.r[0].m128_f32[1];
		desc2.Transform[0][2] = transformMat.r[0].m128_f32[2];
		desc2.Transform[0][3] = transformMat.r[0].m128_f32[3];

		desc2.Transform[1][0] = transformMat.r[1].m128_f32[0];
		desc2.Transform[1][1] = transformMat.r[1].m128_f32[1];
		desc2.Transform[1][2] = transformMat.r[1].m128_f32[2];
		desc2.Transform[1][3] = transformMat.r[1].m128_f32[3];

		desc2.Transform[2][0] = transformMat.r[2].m128_f32[0];
		desc2.Transform[2][1] = transformMat.r[2].m128_f32[1];
		desc2.Transform[2][2] = transformMat.r[2].m128_f32[2];
		desc2.Transform[2][3] = transformMat.r[2].m128_f32[3];

		/*desc2.Transform[0][0] = uInv.x;
		desc2.Transform[0][1] = uInv.y;
		desc2.Transform[0][2] = uInv.z;

		desc2.Transform[1][0] = vInv.x;
		desc2.Transform[1][1] = vInv.y;
		desc2.Transform[1][2] = vInv.z;

		desc2.Transform[2][0] = wInv.x;
		desc2.Transform[2][1] = wInv.y;
		desc2.Transform[2][2] = wInv.z;*/

		desc2.Transform[0][3] = m_offsets[i].x;
		desc2.Transform[1][3] = m_offsets[i].y;
		desc2.Transform[2][3] = m_offsets[i].z;

		/*desc2.Transform[0][0] = quadMat.r[0].m128_f32[0];
		desc2.Transform[0][1] = quadMat.r[0].m128_f32[1];
		desc2.Transform[0][2] = quadMat.r[0].m128_f32[2];
		desc2.Transform[0][3] = quadMat.r[0].m128_f32[3];

		desc2.Transform[1][0] = quadMat.r[1].m128_f32[0];
		desc2.Transform[1][1] = quadMat.r[1].m128_f32[1];
		desc2.Transform[1][2] = quadMat.r[1].m128_f32[2];
		desc2.Transform[1][3] = quadMat.r[1].m128_f32[3];

		desc2.Transform[2][0] = quadMat.r[2].m128_f32[0];
		desc2.Transform[2][1] = quadMat.r[2].m128_f32[1];
		desc2.Transform[2][2] = quadMat.r[2].m128_f32[2];
		desc2.Transform[2][3] = quadMat.r[2].m128_f32[3];*/

		desc2.InstanceMask = 2;
		desc2.InstanceID = i + m_nbrParticles;
		desc2.AccelerationStructure = m_bottomLevelAccelerationStructures[1]->GetGPUVirtualAddress();
		descs[i + m_nbrParticles] = desc2;

		XMVECTOR xNorm = XMVector3Normalize({ desc2.Transform[0][0], desc2.Transform[0][1], desc2.Transform[0][2] });
		XMVECTOR yNorm = XMVector3Normalize({ desc2.Transform[1][0], desc2.Transform[1][1], desc2.Transform[1][2] });
		XMVECTOR zNorm = XMVector3Normalize({ desc2.Transform[2][0], desc2.Transform[2][1], desc2.Transform[2][2] });

		float angleX = acos(x.x * xNorm.m128_f32[0] + x.y * xNorm.m128_f32[1] + x.z * xNorm.m128_f32[2]);
		float angleY = acos(y.x * yNorm.m128_f32[0] + y.y * yNorm.m128_f32[1] + y.z * yNorm.m128_f32[2]);
		float angleZ = acos(z.x * zNorm.m128_f32[0] + z.y * zNorm.m128_f32[1] + z.z * zNorm.m128_f32[2]);

		m_particleRot = { angleX, angleY, angleZ };
	}
	/*D3D12_RAYTRACING_INSTANCE_DESC desc = {};
	desc.Transform[0][0] = desc.Transform[1][1] = desc.Transform[2][2] = 1;
	desc.InstanceMask = 2;
	desc.InstanceID = 1;
	desc.AccelerationStructure = m_bottomLevelAccelerationStructures[1]->GetGPUVirtualAddress();
	descs[m_nbrParticles] = desc;*/
	D3D12_RAYTRACING_INSTANCE_DESC desc = {};
	desc.Transform[0][0] = desc.Transform[1][1] = desc.Transform[2][2] = 1;
	desc.InstanceMask = 3;
	desc.InstanceID = m_nbrParticles * 2;
	desc.AccelerationStructure = m_bottomLevelAccelerationStructures[2]->GetGPUVirtualAddress();
	descs[m_nbrParticles * 2] = desc;

	D3D12_RAYTRACING_INSTANCE_DESC desc2 = {};
	desc2.Transform[0][0] = desc2.Transform[1][1] = desc2.Transform[2][2] = 1;
	desc2.InstanceMask = 4;
	desc2.InstanceID = m_nbrParticles * 2 + 1;
	desc2.AccelerationStructure = m_bottomLevelAccelerationStructures[3]->GetGPUVirtualAddress();
	descs[m_nbrParticles * 2 + 1] = desc2;

	void* pMappedData;
	m_instanceDescs->Map(0, nullptr, &pMappedData);
	memcpy(pMappedData, descs.data(), sizeof(descs[0])* descs.size());
	m_instanceDescs->Unmap(0, nullptr);

	// Get required sizes for an acceleration structure.
	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS topLevelInputs = {};
	topLevelInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
	topLevelInputs.Flags = buildFlags;
	topLevelInputs.NumDescs = nbrDescs;
	topLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
	// Top Level Acceleration Structure desc
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC topLevelBuildDesc = {};
	{
		topLevelInputs.InstanceDescs = m_instanceDescs->GetGPUVirtualAddress();
		topLevelBuildDesc.Inputs = topLevelInputs;
		topLevelBuildDesc.DestAccelerationStructureData = m_topLevelAccelerationStructure->GetGPUVirtualAddress();
		topLevelBuildDesc.ScratchAccelerationStructureData = m_scratchResource->GetGPUVirtualAddress();
	}
	m_gpuTimer.Start(m_commandList.Get(), 5);
	m_commandList->BuildRaytracingAccelerationStructure(&topLevelBuildDesc, 0, nullptr);
	m_gpuTimer.Stop(m_commandList.Get(), 5);


	if (m_currentParticleGroup == m_particleGroupAmount - 1)
	{
		m_currentParticleGroup = 0;
	}
	else
	{
		m_currentParticleGroup++;
	}
	for (auto& particleConstantBufferData : m_particleConstantBufferData)
	{
		particleConstantBufferData = m_particleConstantBufferData[frameIndex];
	}
	m_valuesConstantBufferData[frameIndex].currentParticleGroup = m_currentParticleGroup;
	m_valuesConstantBufferData[frameIndex].particleRot = m_particleRot;
	if ((m_elapsed_seconds - elapsedTime2) >= (1.0f / 30.0f))
	{
		if (m_shouldTexFrameUpdate)
		{
			if (m_texFrame < 127)
			{
				m_texFrame += 1;
			}
			else
			{
				m_texFrame = 0;
			}
		}
		m_valuesConstantBufferData[frameIndex].texFrame = m_texFrame;
		elapsedTime2 = m_elapsed_seconds;
	}
	for (auto& valuesConstantBufferData : m_valuesConstantBufferData)
	{
		valuesConstantBufferData = m_valuesConstantBufferData[frameIndex];
	}
}

void Sample3DSceneRenderer::GenerateParticles()
{
	//float particleRadius = 0.05f;
	//float particleRadius = 0.36056f; //3 * base volume (250 particles)
	//float particleRadius = 0.28618f; //3/2 * base volume (500 particles)
	float particleRadius = 0.25f; //base size (750 particles)
	//float particleRadius = 0.22714f; //3/4 * base volume (1000 particles)
	//float particleRadius = 0.21086f; //3/5 * base volume (1250 particles)
	//float particleRadius = 0.19843f; //1/2 * base volume (1500 particles)
	float radius = 0.75f;
	float tubeLength = 4.0f;
	//float sphereRadius = 1.5f;

	Icosahedron ico(particleRadius);

	m_particleVertexBufferData.resize(ico.getVertexCount());
	m_particleIndexBufferData.resize(ico.getIndexCount());

	UINT64 vIdx = 0;
	std::vector<float> verts(ico.getVertexCount() * 3);
	memcpy(&verts[0], ico.getVertices(), ico.getVertexCount() * 3 * sizeof(float));
	UINT64 nIdx = 0;
	std::vector<float> norms(ico.getNormalCount() * 3);
	memcpy(&norms[0], ico.getNormals(), ico.getNormalCount() * 3 * sizeof(float));
	UINT64 tIdx = 0;
	std::vector<float> texs(ico.getTexCoordCount() * 2);
	memcpy(&texs[0], ico.getTexCoords(), ico.getTexCoordCount() * 2 * sizeof(float));

	for (int i = 0; i < ico.getVertexCount(); i++)
	{
		XMFLOAT3 pos{verts[vIdx++], verts[vIdx++], verts[vIdx++]};
		XMFLOAT3 norm{ norms[nIdx++], norms[nIdx++], norms[nIdx++] };
		XMFLOAT2 tex{ texs[tIdx++], texs[tIdx++] };
		m_particleVertexBufferData[i] = { pos, XMFLOAT3(1.0f, 0.0f, 0.0f), tex, norm };
	}

	UINT64 iIdx = 0;
	std::vector<unsigned int> idxs(ico.getIndexCount());
	memcpy(&idxs[0], ico.getIndices(), ico.getIndexCount() * sizeof(unsigned int));
	for (int i = 0; i < ico.getIndexCount(); i++)
	{
		m_particleIndexBufferData[i] = static_cast<unsigned short>(idxs[i]);
	}

	m_vertexBufferData.insert(m_vertexBufferData.end(), m_particleVertexBufferData.begin(), m_particleVertexBufferData.end());
	m_indexBufferData.insert(m_indexBufferData.end(), m_particleIndexBufferData.begin(), m_particleIndexBufferData.end());

	m_quadParticleVertexBufferData.resize(4);
	m_quadParticleIndexBufferData.resize(6);
	CreateQuad(m_quadParticleVertexBufferData, m_quadParticleIndexBufferData, particleRadius * 4.0f , particleRadius * 4.0f, 0, 0, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f });

	for (auto& data : m_quadParticleVertexBufferData)
	{
		//XMMATRIX rotate = XMMatrixRotationY(XM_PI);
		XMMATRIX rotate2 = XMMatrixRotationZ(XM_PI);
		XMVECTOR vPos = XMLoadFloat3(&data.pos);
		XMVECTOR vNorm = XMLoadFloat3(&data.normal);
		vPos.m128_f32[0] -= particleRadius;
		vPos.m128_f32[1] -= particleRadius;
		//vPos = XMVector3Transform(vPos, rotate);
		//vNorm = XMVector3Transform(vNorm, rotate);
		vPos = XMVector3Transform(vPos, rotate2);
		vNorm = XMVector3Transform(vNorm, rotate2);
		//vPos.m128_f32[1] += 3.0f;
		XMStoreFloat3(&data.pos, vPos);
		XMStoreFloat3(&data.normal, vNorm);
	}

	m_vertexBufferData.insert(m_vertexBufferData.end(), m_quadParticleVertexBufferData.begin(), m_quadParticleVertexBufferData.end());
	m_indexBufferData.insert(m_indexBufferData.end(), m_quadParticleIndexBufferData.begin(), m_quadParticleIndexBufferData.end());

	//srand(static_cast <unsigned> (time(0)));
	srand(123);
	m_offsets.resize(m_nbrParticles);
	m_motionVectors.resize(m_nbrParticles);
#ifdef RANDOM_PARTICLE_POS
	int count = 0;
	float radiusradius = radius * radius;
	while (count < m_nbrParticles)
	{
		float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (radius * 2.0f))) - radius;
		float y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (radius * 2.0f))) - radius;
		float z = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / tubeLength)) - tubeLength / 2.0f;

		if (x * x + y * y > radiusradius)
		{
			continue;
		}

		XMFLOAT3 offset{};
		if (count % 2 == 0) {
			offset = XMFLOAT3(x,
				y + 1.0f,
				z);
		}
		else
		{
			XMMATRIX rotate = XMMatrixRotationY(XM_PIDIV2);
			XMVECTOR vOffset = XMVECTOR{ x,
				y - 1.0f,
				z };
			vOffset = XMVector3Transform(vOffset, rotate);
			XMStoreFloat3(&offset, vOffset);
		}
		m_offsets[count] = offset;

		float xMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		float yMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		float zMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		m_motionVectors[count] = XMFLOAT3(xMotion, yMotion, zMotion);

		count++;
	}
#else
	int count = 0;
	float xStepSize = particleWidth * 2.0f + 0.005f;
	float yStepSize = particleWidth * 2.0f + 0.005f;
	float zStepSize = particleWidth * 2.0f + 0.005f;
	float x = -radius;
	float y = -radius;
	float z = -tubeLength / 2.0f;
	float coordOffset = 0.0f;
	float radiusradius = radius * radius;
	while (count < m_nbrParticles)
	{
		if (x * x + y * y <= radiusradius)
		{
			XMFLOAT3 offset{};
			if (count % 2 == 0)
			{
				offset = XMFLOAT3(x,
					y + 1.0f,
					z);
			}
			else
			{
				XMMATRIX rotate = XMMatrixRotationY(XM_PIDIV2);
				XMVECTOR vOffset = XMVECTOR{ x,
					y - 1.0f,
					z };
				vOffset = XMVector3Transform(vOffset, rotate);
				XMStoreFloat3(&offset, vOffset);
			}
			m_offsets[count] = offset;

			float xMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
			float yMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
			float zMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
			m_motionVectors[count] = XMFLOAT3(xMotion, yMotion, zMotion);

			count++;
			if (count % 2 == 0)
			{
				continue;
			}
		}

		if (x < radius)
		{
			x += xStepSize;
		}
		else
		{
			x = -radius + coordOffset;
			if (y < radius)
			{
				y += yStepSize;
			}
			else
			{
				y = -radius + coordOffset;
				if (z < tubeLength / 2.0f)
				{
					z += zStepSize;
				}
				else
				{
					z = -tubeLength / 2.0f;
					coordOffset += particleWidth + 0.005f;
					if (coordOffset >= particleWidth * 2.0f)
					{
						coordOffset -= (particleWidth * 2.0f);
					}
					x = -radius + coordOffset;
					y = -radius + coordOffset;
					z += coordOffset;
				}
			}
		}
	}
#endif
	/*int count = 0;
	float radiusradius = (sphereRadius - particleRadius - 0.001f) * (sphereRadius - particleRadius - 0.001f);
	while (count < m_nbrParticles)
	{
		float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (sphereRadius * 2.0f))) - sphereRadius;
		float y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (sphereRadius * 2.0f))) - sphereRadius;
		float z = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (sphereRadius * 2.0f))) - sphereRadius;

		if (x * x + y * y + z * z > radiusradius)
		{
			continue;
		}

		XMFLOAT3 offset = XMFLOAT3(x, y, z);
		m_offsets[count] = offset;

		float xMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		float yMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		float zMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
		m_motionVectors[count] = XMFLOAT3(xMotion, yMotion, zMotion);

		count++;
	}*/

	/*int count = 0;
	XMFLOAT3 offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_offsets[count] = offset;

	float xMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
	float yMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
	float zMotion = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 2.0f) - 1.0f;
	m_motionVectors[count] = XMFLOAT3(xMotion, yMotion, zMotion);*/

	/*int count = 0;
	float spawnRadius = particleRadius;
	m_motionVelocities.resize(m_nbrParticles);
	m_rotSpeeds.resize(m_nbrParticles);
	m_scalingSpeeds.resize(m_nbrParticles);
	while (count < m_nbrParticles)
	{
		float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (spawnRadius * 2.0f))) - spawnRadius;
		float y = -1.5f;
		float z = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (spawnRadius * 2.0f))) - spawnRadius;

		if (x * x + z * z > spawnRadius)
		{
			continue;
		}
		XMFLOAT3 offset = XMFLOAT3(x, y, z);
		m_offsets[count] = offset;

		float coneRadius = particleRadius * 4.0f;
		float coneLength = 3.0f;
		float coneEnd = coneLength + y;

		float xMotion = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (coneRadius * 2.0f))) - coneRadius;
		xMotion -= coneRadius;
		float yMotion = coneEnd;
		float zMotion = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (coneRadius * 2.0f))) - coneRadius;
		m_motionVectors[count] = XMFLOAT3(xMotion, yMotion, zMotion);

		float velocity = static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 0.5f) + 0.5f;
		velocity /= 10.0f;
		m_motionVelocities[count] = velocity;

		float rotSpeed = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (XM_PIDIV2 * 2.0f))) - XM_PIDIV2;
		rotSpeed /= 2.0f;
		m_rotSpeeds[count] = rotSpeed;

		float scaleSpeed = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		scaleSpeed /= 10.0f;
		m_scalingSpeeds[count] = scaleSpeed;

		count++;
	}*/
}

void Sample3DSceneRenderer::HandleKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e)
{
	auto vKey = e->VirtualKey;
	if (vKey.ToString()->Equals("W"))
	{
		m_wKey = true;
	}
	if (vKey.ToString()->Equals("A"))
	{
		m_aKey = true;
	}
	if (vKey.ToString()->Equals("S"))
	{
		m_sKey = true;
	}
	if (vKey.ToString()->Equals("D"))
	{
		m_dKey = true;
	}
	if (vKey.ToString()->Equals("Q"))
	{
		m_qKey = true;
	}
	if (vKey.ToString()->Equals("E"))
	{
		m_eKey = true;
	}
	if (vKey.ToString()->Equals("Space"))
	{
		m_shouldLightRotate = !m_shouldLightRotate;
	}
	if (vKey.ToString()->Equals("Up"))
	{
		m_upKey = true;
	}
	if (vKey.ToString()->Equals("Down"))
	{
		m_downKey = true;
	}
	if (vKey.ToString()->Equals("Left"))
	{
		m_leftKey = true;
	}
	if (vKey.ToString()->Equals("Right"))
	{
		m_rightKey = true;
	}
	if (vKey.ToString()->Equals("T"))
	{
		m_basic_mode = true;
		m_simple_shadow_mode = false;
		m_full_optical_depth_mode = false;

		auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
		m_valuesConstantBufferData[frameIndex].basic_mode = 1;
		m_valuesConstantBufferData[frameIndex].simple_shadow_mode = 0;
		m_valuesConstantBufferData[frameIndex].full_optical_depth_mode = 0;

		for (auto& valuesConstantBufferData : m_valuesConstantBufferData)
		{
			valuesConstantBufferData = m_valuesConstantBufferData[frameIndex];
		}
	}
	if (vKey.ToString()->Equals("Y"))
	{
		m_basic_mode = false;
		m_simple_shadow_mode = true;
		m_full_optical_depth_mode = false;

		auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
		m_valuesConstantBufferData[frameIndex].basic_mode = 0;
		m_valuesConstantBufferData[frameIndex].simple_shadow_mode = 1;
		m_valuesConstantBufferData[frameIndex].full_optical_depth_mode = 0;

		for (auto& valuesConstantBufferData : m_valuesConstantBufferData)
		{
			valuesConstantBufferData = m_valuesConstantBufferData[frameIndex];
		}
	}
	if (vKey.ToString()->Equals("U"))
	{
		m_basic_mode = false;
		m_simple_shadow_mode = false;
		m_full_optical_depth_mode = true;

		auto frameIndex = m_deviceResources->GetCurrentFrameIndex();
		m_valuesConstantBufferData[frameIndex].basic_mode = 0;
		m_valuesConstantBufferData[frameIndex].simple_shadow_mode = 0;
		m_valuesConstantBufferData[frameIndex].full_optical_depth_mode = 1;

		for (auto& valuesConstantBufferData : m_valuesConstantBufferData)
		{
			valuesConstantBufferData = m_valuesConstantBufferData[frameIndex];
		}
	}
	if (vKey.ToString()->Equals("I"))
	{
		m_shouldTexFrameUpdate = !m_shouldTexFrameUpdate;
	}
}

void Sample3DSceneRenderer::HandleKeyUp(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e)
{
	auto vKey = e->VirtualKey;
	if (vKey.ToString()->Equals("W"))
	{
		m_wKey = false;
	}
	if (vKey.ToString()->Equals("A"))
	{
		m_aKey = false;
	}
	if (vKey.ToString()->Equals("S"))
	{
		m_sKey = false;
	}
	if (vKey.ToString()->Equals("D"))
	{
		m_dKey = false;
	}
	if (vKey.ToString()->Equals("Q"))
	{
		m_qKey = false;
	}
	if (vKey.ToString()->Equals("E"))
	{
		m_eKey = false;
	}
	if (vKey.ToString()->Equals("Up"))
	{
		m_upKey = false;
	}
	if (vKey.ToString()->Equals("Down"))
	{
		m_downKey = false;
	}
	if (vKey.ToString()->Equals("Left"))
	{
		m_leftKey = false;
	}
	if (vKey.ToString()->Equals("Right"))
	{
		m_rightKey = false;
	}
}

// From D3D12RaytracingHelloWorld example
// Compute the average frames per second and million rays per second.
void Sample3DSceneRenderer::CalculateFrameStats()
{
	static int frameCnt = 0;
	static double elapsedTime = 0.0f;
	static float frameTimeOpticalDepth = 0.0f;
	static float frameTimeViewRays = 0.0f;
	static float frameTimeCompute = 0.0f;
	static float frameTimeClearCompute = 0.0f;
	static float frameTimeTotal = 0.0f;
	static float frameTimeBuildAS = 0.0f;
	frameTimeOpticalDepth += static_cast<float>(m_gpuTimer.GetElapsedMS(0));
	frameTimeViewRays += static_cast<float>(m_gpuTimer.GetElapsedMS(1));
	frameTimeCompute += static_cast<float>(m_gpuTimer.GetElapsedMS(2));
	frameTimeClearCompute += static_cast<float>(m_gpuTimer.GetElapsedMS(3));
	frameTimeTotal += static_cast<float>(m_gpuTimer.GetElapsedMS(4));
	frameTimeBuildAS += static_cast<float>(m_gpuTimer.GetElapsedMS(5));
	frameCnt++;
	float width = m_deviceResources->GetScreenViewport().Width;
	float height = m_deviceResources->GetScreenViewport().Height;

	static std::wstringstream csvContents;
	float frameTimeOpticalDepthcsv = static_cast<float>(m_gpuTimer.GetElapsedMS(0));
	float frameTimeViewRayscsv = static_cast<float>(m_gpuTimer.GetElapsedMS(1));
	float frameTimeComputecsv = static_cast<float>(m_gpuTimer.GetElapsedMS(2));
	float frameTimeClearComputecsv = static_cast<float>(m_gpuTimer.GetElapsedMS(3));
	float frameTimeTotalcsv = static_cast<float>(m_gpuTimer.GetElapsedMS(4));
	float frameTimeBuildAScsv = static_cast<float>(m_gpuTimer.GetElapsedMS(5));

	csvContents //<< std::setprecision(2)
		<< frameTimeBuildAScsv << L","
		<< frameTimeOpticalDepthcsv << L","
		<< frameTimeComputecsv << L","
		<< frameTimeClearComputecsv << L","
		<< frameTimeViewRayscsv << L","
		<< frameTimeTotalcsv << L"\n";

	// Compute averages over one second period.
	if ((m_elapsed_seconds - elapsedTime) >= 1.0f)
	{
		float diff = static_cast<float>(m_elapsed_seconds - elapsedTime);
		float fps = static_cast<float>(frameCnt) / diff; // Normalize to an exact second.
		float raytracingTime1 = frameTimeOpticalDepth / static_cast<float>(frameCnt);
		float raytracingTime2 = frameTimeViewRays / static_cast<float>(frameCnt);
		float computeTime = frameTimeCompute / static_cast<float>(frameCnt);
		float computeTime2 = frameTimeClearCompute / static_cast<float>(frameCnt);
		float totalTime = frameTimeTotal / static_cast<float>(frameCnt);
		float buildASTime = frameTimeBuildAS / static_cast<float>(frameCnt);

		frameCnt = 0;
		elapsedTime = m_elapsed_seconds;
		frameTimeOpticalDepth = 0.0f;
		frameTimeViewRays = 0.0f;
		frameTimeCompute = 0.0f;
		frameTimeClearCompute = 0.0f;
		frameTimeTotal = 0.0f;
		frameTimeBuildAS = 0.0f;

		float MRaysPerSecond = (width * height * fps) / static_cast<float>(1e6);

		std::wstringstream windowText;

		windowText << L"fps: " << fps
			//<< L"     ~Million Primary Rays/s: " << MRaysPerSecond
			<< std::setprecision(2)
			<< L" Build AS: " << buildASTime << "ms"
			<< L" Optical depth pass: " << raytracingTime1 << "ms"
			<< L" Compute pass: " << computeTime << "ms"
			<< L" Compute pass 2: " << computeTime2 << "ms"
			<< L" View pass: " << raytracingTime2 << "ms"
			<< L" Total time: " << totalTime << "ms";

		auto appView = ApplicationView::GetForCurrentView();

		m_windowText = windowText.str() + L"\n";
		std::wstring windowTextStr = windowText.str();
		const wchar_t* w_chars = windowTextStr.c_str();
		Platform::String^ x = ref new Platform::String(w_chars);
		appView->Title = x;

		if (m_fileComplete)
		{
			auto fileTask = create_task(KnownFolders::GetFolderForUserAsync(nullptr /* current user */, KnownFolderId::PicturesLibrary));

			auto fileTask2 = (fileTask).then([this](StorageFolder^ picturesFolder)
				{
					return picturesFolder->GetFileAsync("performanceStats.txt");
				});

			auto fileTask3 = (fileTask2).then([this](StorageFile^ file)
				{
					Platform::String^ text = ref new Platform::String(m_windowText.c_str());
					FileIO::AppendTextAsync(file, text);
				});
		}
		if (m_fileComplete2)
		{
			m_csvText = csvContents.str();
			csvContents.str(L"");
			auto fileTask = create_task(KnownFolders::GetFolderForUserAsync(nullptr /* current user */, KnownFolderId::PicturesLibrary));

			auto fileTask2 = (fileTask).then([this](StorageFolder^ picturesFolder)
				{
					return picturesFolder->GetFileAsync("performanceStats.csv");
				});

			auto fileTask3 = (fileTask2).then([this](StorageFile^ file)
				{
					Platform::String^ text = ref new Platform::String(m_csvText.c_str());
					FileIO::AppendTextAsync(file, text);
				});
		}
	}
}

void Sample3DSceneRenderer::CreateSphere(std::vector<VertexPositionColor>& sphereVertices, std::vector<unsigned short>& sphereIndices, float radius, unsigned int const longitude_split_count, unsigned int const latitude_split_count, float const offset, XMFLOAT3 color)
{
	auto const longitude_slice_edges_count = longitude_split_count + 1u;
	auto const latitude_slice_edges_count = latitude_split_count + 1u;
	auto const longitude_slice_vertices_count = longitude_slice_edges_count + 1u;
	auto const latitude_slice_vertices_count = latitude_slice_edges_count + 1u;
	auto const vertices_nb = longitude_slice_vertices_count * latitude_slice_vertices_count;

	auto vertices = std::vector<XMFLOAT3>(vertices_nb);
	auto normals = std::vector<XMVECTOR>(vertices_nb);
	auto texcoords = std::vector<XMFLOAT2>(vertices_nb);
	auto tangents = std::vector<XMVECTOR>(vertices_nb);
	auto binormals = std::vector<XMVECTOR>(vertices_nb);

	//float const spread_start = radius - 0.5f * spread_length;
	float const d_theta = XM_2PI / (static_cast<float>(longitude_slice_edges_count));
	float const d_phi = XM_PI / (static_cast<float>(latitude_slice_edges_count));

	// generate vertices iteratively
	size_t index = 0u;
	float theta = 0.0f;
	float phi = 0.0f;
	for (unsigned int i = 0u; i < longitude_slice_vertices_count; ++i) {
		float const cos_theta = std::cos(theta);
		float const sin_theta = std::sin(theta);

		phi = 0.0f;
		//float distance_to_centre = spread_start;
		for (unsigned int j = 0u; j < latitude_slice_vertices_count; ++j) {
			float const cos_phi = std::cos(phi);
			float const sin_phi = std::sin(phi);

			// vertex
			vertices[index] = XMFLOAT3(radius * sin_theta * sin_phi - offset,
				-radius * cos_phi,
				radius * cos_theta * sin_phi);

			// texture coordinates
			texcoords[index] = XMFLOAT2(static_cast<float>(i) / (static_cast<float>(longitude_slice_vertices_count)),
				static_cast<float>(j) / (static_cast<float>(latitude_slice_vertices_count)));

			// tangent
			auto const t = XMVECTOR{ cos_theta, 0.0f, -sin_theta };
			tangents[index] = t;

			// binormal
			auto b = XMVECTOR{ radius * sin_theta * cos_phi, radius * sin_phi, radius * cos_theta * cos_phi };
			b = XMVector3Normalize(b);
			binormals[index] = b;

			// normal
			auto const n = XMVector3Cross(t, b);
			normals[index] = n;

			//distance_to_centre += d_phi;
			++index;

			phi += d_phi;
		}

		theta += d_theta;
	}
	//std::vector<VertexPositionColor> sphereVertices(index);
	/*auto old_size = sphereVertices.size();
	auto new_size = old_size + index;
	sphereVertices.resize(new_size);
	UINT idx = old_size;
	for (int i = 0; i < index; i++)
	{
		XMFLOAT3 norms;
		XMStoreFloat3(&norms, normals[i]);
		sphereVertices[idx++] = { vertices[i], color, texcoords[i], norms };
	}*/
	for (int i = 0; i < index; i++)
	{
		sphereVertices[m_vertexIdx++] = { vertices[i], color, texcoords[i], XMFLOAT3(0.0f, 0.0f, 1.0f) };
	}

	// create index array
	auto index_sets = std::vector<XMUINT3>(2u * longitude_slice_edges_count * latitude_slice_edges_count);

	// generate indices iteratively
	index = 0u;
	for (unsigned int i = 0u; i < longitude_slice_edges_count; ++i)
	{
		for (unsigned int j = 0u; j < latitude_slice_edges_count; ++j)
		{
			/*index_sets[index] = XMUINT3(latitude_slice_vertices_count * (i + 0u) + (j + 0u) + old_size,
				latitude_slice_vertices_count * (i + 1u) + (j + 0u) + old_size,
				latitude_slice_vertices_count * (i + 0u) + (j + 1u) + old_size);
			++index;

			index_sets[index] = XMUINT3(latitude_slice_vertices_count * (i + 0u) + (j + 1u) + old_size,
				latitude_slice_vertices_count * (i + 1u) + (j + 0u) + old_size,
				latitude_slice_vertices_count * (i + 1u) + (j + 1u) + old_size);
			++index;*/
			index_sets[index] = XMUINT3(latitude_slice_vertices_count * (i + 0u) + (j + 0u),
				latitude_slice_vertices_count * (i + 1u) + (j + 0u),
				latitude_slice_vertices_count * (i + 0u) + (j + 1u));
			++index;

			index_sets[index] = XMUINT3(latitude_slice_vertices_count * (i + 0u) + (j + 1u),
				latitude_slice_vertices_count * (i + 1u) + (j + 0u),
				latitude_slice_vertices_count * (i + 1u) + (j + 1u));
			++index;
		}
	}
	//std::vector<unsigned short> sphereIndices(index * 3);
	/*auto old_size2 = sphereIndices.size();
	auto new_size2 = old_size2 + index * 3;
	sphereIndices.resize(new_size2);
	UINT idx2 = old_size2;
	for (int i = 0; i < index; i++)
	{
		sphereIndices[idx2++] = index_sets[i].x;
		sphereIndices[idx2++] = index_sets[i].y;
		sphereIndices[idx2++] = index_sets[i].z;
	}*/
	for (int i = 0; i < index; i++)
	{
		sphereIndices[m_indexIdx++] = index_sets[i].x;
		sphereIndices[m_indexIdx++] = index_sets[i].y;
		sphereIndices[m_indexIdx++] = index_sets[i].z;
	}
}

void Sample3DSceneRenderer::CreateQuad(std::vector<VertexPositionColor>& quadVertices, std::vector<unsigned short>& quadIndices, float const width, float const height, unsigned int const horizontal_split_count, unsigned int const vertical_split_count, XMFLOAT3 offset, XMFLOAT3 color)
{
	auto const horizontal_slice_edges_count = horizontal_split_count + 1u;
	auto const vertical_slice_edges_count = vertical_split_count + 1u;
	auto const horizontal_slice_vertices_count = horizontal_slice_edges_count + 1u;
	auto const vertical_slice_vertices_count = vertical_slice_edges_count + 1u;
	auto const vertices_nb = horizontal_slice_vertices_count * vertical_slice_vertices_count;

	auto vertices = std::vector<XMFLOAT3>(vertices_nb);
	auto texcoords = std::vector<XMFLOAT2>(vertices_nb);

	// generate vertices iteratively
	size_t index = 0u;
	for (unsigned int i = 0u; i < horizontal_slice_vertices_count; ++i) {
		for (unsigned int j = 0u; j < vertical_slice_vertices_count; ++j) {

			// vertex
			vertices[index] = XMFLOAT3(width * static_cast<float>(i) / static_cast<float>(horizontal_slice_vertices_count) + offset.x,
				height * static_cast<float>(j) / static_cast<float>(vertical_slice_vertices_count) + offset.y,
				0.0f + offset.z);

			// texture coordinates
			texcoords[index] = XMFLOAT2(static_cast<float>(i) / (static_cast<float>(horizontal_slice_vertices_count) - 1.0f),
				static_cast<float>(j) / (static_cast<float>(vertical_slice_vertices_count) - 1.0f));

			++index;
		}
	}

	//auto old_size = quadVertices.size();
	//auto new_size = old_size + index;
	//quadVertices.resize(new_size);
	//UINT idx = old_size;
	for (int i = 0; i < index; i++)
	{
		quadVertices[i] = { vertices[i], color, texcoords[i], XMFLOAT3(0.0f, 0.0f, 1.0f) };
	}

	// create index array
	auto index_sets = std::vector<XMUINT3>(2u * horizontal_slice_edges_count * vertical_slice_edges_count);

	// generate indices iteratively
	index = 0u;
	for (unsigned int i = 0u; i < horizontal_slice_edges_count; ++i)
	{
		for (unsigned int j = 0u; j < vertical_slice_edges_count; ++j)
		{
			/*index_sets[index] = XMUINT3(vertical_slice_vertices_count * (i + 0u) + (j + 0u) + old_size,
				vertical_slice_vertices_count * (i + 0u) + (j + 1u) + old_size,
				vertical_slice_vertices_count * (i + 1u) + (j + 0u) + old_size);
			++index;

			index_sets[index] = XMUINT3(vertical_slice_vertices_count * (i + 0u) + (j + 1u) + old_size,
				vertical_slice_vertices_count * (i + 1u) + (j + 1u) + old_size,
				vertical_slice_vertices_count * (i + 1u) + (j + 0u) + old_size);
			++index;*/
			index_sets[index] = XMUINT3(vertical_slice_vertices_count * (i + 0u) + (j + 0u),
				vertical_slice_vertices_count * (i + 0u) + (j + 1u),
				vertical_slice_vertices_count * (i + 1u) + (j + 0u));
			++index;

			index_sets[index] = XMUINT3(vertical_slice_vertices_count * (i + 0u) + (j + 1u),
				vertical_slice_vertices_count * (i + 1u) + (j + 1u),
				vertical_slice_vertices_count * (i + 1u) + (j + 0u));
			++index;
		}
	}

	//auto old_size2 = quadIndices.size();
	//auto new_size2 = old_size2 + index * 3;
	//quadIndices.resize(new_size2);
	//UINT idx2 = old_size2;
	UINT idx = 0;
	for (int i = 0; i < index; i++)
	{
		quadIndices[idx++] = index_sets[i].x;
		quadIndices[idx++] = index_sets[i].y;
		quadIndices[idx++] = index_sets[i].z;
	}
}

/*void Sample3DSceneRenderer::CreateTexture()
{
	//Create and Upload Texture
	{
		UINT texWidth = 128;
		UINT texHeight = 128;
		UINT texStride = 4;
		UINT texOffset = 0;
		std::vector<UINT8> pixels;
		pixels.resize(texWidth * texHeight * 4);
		for (int i = 0; i < texWidth * texHeight; i++)
		{
			int row = i / texWidth;
			int col = i % texWidth;
			int y = row - texHeight / 2;
			int x = col - texWidth / 2;
			if ((y * y + x * x) <= ((texWidth / 2) * (texWidth / 2)))
			{
				pixels[i * texStride] = 128;
				pixels[i * texStride + 1] = 128;
				pixels[i * texStride + 2] = 128;
				pixels[i * texStride + 3] = 9; // range from 0 to 255, 255==1 0==0
			}
			else {
				pixels[i * texStride] = 0;
				pixels[i * texStride + 1] = 0;
				pixels[i * texStride + 2] = 0;
				pixels[i * texStride + 3] = 0;
			}*/

			/*if (i < texWidth * texHeight / 2)
			{
				if (i % texWidth < texWidth / 2)
				{
					pixels[i * texStride] = 0;
					pixels[i * texStride + 1] = 0;
					pixels[i * texStride + 2] = 0;
					pixels[i * texStride + 3] = 0xFF;
				}
				else
				{
					pixels[i * texStride] = 0xFF;
					pixels[i * texStride + 1] = 0;
					pixels[i * texStride + 2] = 0xFF;
					pixels[i * texStride + 3] = 0xFF;
				}
			}
			else
			{
				if (i % texWidth >= texWidth / 2)
				{
					pixels[i * texStride] = 0;
					pixels[i * texStride + 1] = 0;
					pixels[i * texStride + 2] = 0;
					pixels[i * texStride + 3] = 0xFF;
				}
				else
				{
					pixels[i * texStride] = 0xFF;
					pixels[i * texStride + 1] = 0;
					pixels[i * texStride + 2] = 0xFF;
					pixels[i * texStride + 3] = 0xFF;
				}
			}*/
		/*}

		// Describe the texture
		D3D12_RESOURCE_DESC textureDesc = {};
		textureDesc.Width = texWidth;
		textureDesc.Height = texHeight;
		textureDesc.MipLevels = 1;
		textureDesc.DepthOrArraySize = 1;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

		D3D12_HEAP_PROPERTIES DefaultHeapProperties =
		{
			D3D12_HEAP_TYPE_DEFAULT,
			D3D12_CPU_PAGE_PROPERTY_UNKNOWN,
			D3D12_MEMORY_POOL_UNKNOWN,
			0, 0
		};

		// Create the texture resource
		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(&DefaultHeapProperties, D3D12_HEAP_FLAG_NONE, &textureDesc, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_PPV_ARGS(&m_texture)));
		m_texture->SetName(L"Texture");

		// Describe the resource
		D3D12_RESOURCE_DESC resourceDesc = {};
		resourceDesc.Width = (texWidth * texHeight * texStride);
		resourceDesc.Height = 1;
		resourceDesc.DepthOrArraySize = 1;
		resourceDesc.MipLevels = 1;
		resourceDesc.SampleDesc.Count = 1;
		resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
		resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;

		D3D12_HEAP_PROPERTIES UploadHeapProperties =
		{
			D3D12_HEAP_TYPE_UPLOAD,
			D3D12_CPU_PAGE_PROPERTY_UNKNOWN,
			D3D12_MEMORY_POOL_UNKNOWN,
			0, 0
		};

		// Create the upload heap
		DX::ThrowIfFailed(m_dxrDevice->CreateCommittedResource(&UploadHeapProperties, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&m_textureUploadResource)));
		m_textureUploadResource->SetName(L"Texture Upload Buffer");

		// Upload the texture to the GPU
		// Copy the pixel data to the upload heap resource
		UINT8* pData;
		DX::ThrowIfFailed(m_textureUploadResource->Map(0, nullptr, reinterpret_cast<void**>(&pData)));
		memcpy(pData, pixels.data(), texWidth * texHeight * texStride);
		m_textureUploadResource->Unmap(0, nullptr);

		// Describe the upload heap resource location for the copy
		D3D12_SUBRESOURCE_FOOTPRINT subresource = {};
		subresource.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		subresource.Width = texWidth;
		subresource.Height = texHeight;
		subresource.RowPitch = (texWidth * texStride);
		subresource.Depth = 1;

		D3D12_PLACED_SUBRESOURCE_FOOTPRINT footprint = {};
		footprint.Offset = texOffset;
		footprint.Footprint = subresource;

		D3D12_TEXTURE_COPY_LOCATION source = {};
		source.pResource = m_textureUploadResource.Get();
		source.PlacedFootprint = footprint;
		source.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;

		// Describe the default heap resource location for the copy
		D3D12_TEXTURE_COPY_LOCATION destination = {};
		destination.pResource = m_texture.Get();
		destination.SubresourceIndex = 0;
		destination.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;

		// Copy the buffer resource from the upload heap to the texture resource on the default heap
		m_commandList->CopyTextureRegion(&destination, 0, 0, 0, &source, nullptr);

		// Transition the texture to a shader resource
		D3D12_RESOURCE_BARRIER barrier = {};
		barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		barrier.Transition.pResource = m_texture.Get();
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
		barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
		barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

		m_commandList->ResourceBarrier(1, &barrier);
	}
	// Create the material texture SRV
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC textureSRVDesc = {};
		textureSRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureSRVDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		textureSRVDesc.Texture2D.MipLevels = 1;
		textureSRVDesc.Texture2D.MostDetailedMip = 0;
		textureSRVDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

		auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
		UINT descriptorIndexToUse = m_descriptorsAllocated++;
		m_textureResourceSRVCpuDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, descriptorIndexToUse, m_cbvDescriptorSize);
		UINT descriptorIndexTex = descriptorIndexToUse;
		m_dxrDevice->CreateShaderResourceView(m_texture.Get(), &textureSRVDesc, m_textureResourceSRVCpuDescriptor);
		m_textureResourceSRVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndexTex, m_cbvDescriptorSize);
	}
}*/

void Sample3DSceneRenderer::CreateTexture()
{
	ResourceUploadBatch upload(m_dxrDevice.Get());
	upload.Begin();
	DX::ThrowIfFailed(
		CreateDDSTextureFromFile(
			m_dxrDevice.Get(),
			upload,
			//L"Assets/6PL_BigSmoke_SingleFrame_Pos.dds",
			L"Assets/6PL_BigSmokeAni_Noloop_16x8_Pos.dds",
			&m_texture
		)
	);

	D3D12_SHADER_RESOURCE_VIEW_DESC textureSRVDesc = {};
	textureSRVDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	textureSRVDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	textureSRVDesc.Texture2D.MipLevels = 1;
	textureSRVDesc.Texture2D.MostDetailedMip = 0;
	textureSRVDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	auto descriptorHeapCpuBase = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
	UINT descriptorIndexToUse = m_descriptorsAllocated++;
	m_textureResourceSRVCpuDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, descriptorIndexToUse, m_cbvDescriptorSize);
	UINT descriptorIndexTex = descriptorIndexToUse;
	m_dxrDevice->CreateShaderResourceView(m_texture.Get(), &textureSRVDesc, m_textureResourceSRVCpuDescriptor);
	m_textureResourceSRVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndexTex, m_cbvDescriptorSize);

	auto finish = upload.End(m_deviceResources->GetCommandQueue());
	finish.wait();


	ResourceUploadBatch upload2(m_dxrDevice.Get());
	upload2.Begin();
	DX::ThrowIfFailed(
		CreateDDSTextureFromFile(
			m_dxrDevice.Get(),
			upload2,
			//L"Assets/6PL_BigSmoke_SingleFrame_Neg.dds",
			L"Assets/6PL_BigSmokeAni_Noloop_16x8_Neg.dds",
			&m_texture2
		)
	);

	D3D12_SHADER_RESOURCE_VIEW_DESC textureSRVDesc2 = {};
	textureSRVDesc2.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	textureSRVDesc2.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	textureSRVDesc2.Texture2D.MipLevels = 1;
	textureSRVDesc2.Texture2D.MostDetailedMip = 0;
	textureSRVDesc2.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	auto descriptorHeapCpuBase2 = m_cbvHeap->GetCPUDescriptorHandleForHeapStart();
	UINT descriptorIndexToUse2 = m_descriptorsAllocated++;
	m_textureResourceSRVCpuDescriptor2 = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase2, descriptorIndexToUse2, m_cbvDescriptorSize);
	UINT descriptorIndexTex2 = descriptorIndexToUse2;
	m_dxrDevice->CreateShaderResourceView(m_texture2.Get(), &textureSRVDesc2, m_textureResourceSRVCpuDescriptor2);
	m_textureResourceSRVGpuDescriptor2 = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndexTex2, m_cbvDescriptorSize);

	auto finish2 = upload2.End(m_deviceResources->GetCommandQueue());
	finish2.wait();
}
