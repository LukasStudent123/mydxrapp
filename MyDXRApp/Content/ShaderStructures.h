﻿#pragma once
#ifdef RAYTRACER
typedef float4x4 XMFLOAT4X4;
typedef float3 XMFLOAT3;
typedef float3 XMVECTOR;
typedef float4x4 XMMATRIX;
typedef float4 XMFLOAT4;
typedef float2 XMFLOAT2;
typedef uint UINT;
#else
using namespace DirectX;
#endif

#define MAX_TRACE_RECURSION_DEPTH 2
#define NBR_PARTICLES 750
#define PARTICLE_GROUP_AMOUNT 1
#define OPTICAL_DEPTH_CUTOFF_POINT 60
#define BLOCKSIZE 128

namespace MyDXRApp
{
	// Constant buffer used to send MVP matrices to the vertex shader.
	struct ModelViewProjectionConstantBuffer
	{
		XMFLOAT4X4 model;
		XMFLOAT4X4 view;
		XMFLOAT4X4 projection;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionColor
	{
		XMFLOAT3 pos;
		XMFLOAT3 color;
		XMFLOAT2 texCoords;
		XMFLOAT3 normal;
	};

	struct RayGenData
	{
		XMVECTOR wsCamPos;
		XMVECTOR wsCamU;
		XMVECTOR wsCamV;
		XMVECTOR wsCamW;
		XMMATRIX projectionToWorld;
		XMVECTOR lightPos;
		XMVECTOR lightAmbientColor;
		XMVECTOR lightDiffuseColor;
	};

	struct ParticlePositionData
	{
		//XMFLOAT4 positions[NBR_PARTICLES / PARTICLE_GROUP_AMOUNT];
		XMFLOAT4 positions[NBR_PARTICLES];
	};

	struct ValuesData
	{
		XMFLOAT3 particleRot;
		UINT currentParticleGroup;
		UINT texFrame;
		UINT basic_mode;
		UINT simple_shadow_mode;
		UINT full_optical_depth_mode;
	};

	struct CubeConstantBuffer
	{
		XMFLOAT4	albedo;
	};
}