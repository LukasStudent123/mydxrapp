#define RAYTRACER
#include "ShaderStructures.h"

RWTexture2D<float4> outTex : register(u0); // Output texture
RWStructuredBuffer<float> opticalDepths : register(u1); // Optical Depth buffer
RWStructuredBuffer<float> vecOpticalDepths : register(u2); // vec Optical Depth buffer
RaytracingAccelerationStructure sceneAccelStruct : register(t0, space0); // Scene Acceleration Structure
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<MyDXRApp::VertexPositionColor> Vertices : register(t2, space0);
Texture2D<float4> Tex : register (t3, space0);

ConstantBuffer<MyDXRApp::RayGenData> rayData : register(b0);
ConstantBuffer<MyDXRApp::ParticlePositionData> particlePosData : register(b1);
ConstantBuffer<MyDXRApp::ValuesData> valuesData : register(b2);

[numthreads(BLOCKSIZE, 1, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
	if (DTid.x >= NBR_PARTICLES / PARTICLE_GROUP_AMOUNT)
	{
		return;
	}
	int i, j;
	//int baseIdx = DTid.x * 2 * OPTICAL_DEPTH_CUTOFF_POINT;
	//float particleId = particlePosData.positions[DTid.x].w;
	uint particleId = DTid.x * PARTICLE_GROUP_AMOUNT + valuesData.currentParticleGroup;
	int baseIdx = particleId * 2 * OPTICAL_DEPTH_CUTOFF_POINT;

	// Check if particle is blocked
	if (vecOpticalDepths[baseIdx + (2 * OPTICAL_DEPTH_CUTOFF_POINT) - 2] != 0.0f)
	{
		//opticalDepths[DTid.x] = 999999.0f;
		opticalDepths[particleId] = 999999.0f;
		return;
	}

	float2 hits[OPTICAL_DEPTH_CUTOFF_POINT];
	for (i = 0; i < OPTICAL_DEPTH_CUTOFF_POINT; i++)
	{
		hits[i].x = vecOpticalDepths[baseIdx + (i * 2)];
		hits[i].y = vecOpticalDepths[baseIdx + (i * 2) + 1];
	}

	// Sorting phase
	for (i = 0; i < OPTICAL_DEPTH_CUTOFF_POINT - 1; i++)
	{
		for (j = 0; j < OPTICAL_DEPTH_CUTOFF_POINT - i - 1; j++)
		{
			if (hits[j].x > hits[j + 1].x)
			{
				float2 tmp = hits[j];
				hits[j] = hits[j + 1];
				hits[j + 1] = tmp;
			}
		}
	}

	float outside = 0.0f;
	for (i = 0; i < OPTICAL_DEPTH_CUTOFF_POINT - 1; i++)
	{
		outside += hits[i].y;
	}

	// Calculating optical depth phase
	float inside = -outside;
	float opticalDepth = 1.0f;
	float entryPoint = 0.0f;
	bool isEntryPointSet = true;
	for (i = 0; i < OPTICAL_DEPTH_CUTOFF_POINT; i++)
	{
		if (hits[i].x == 0.0f)
		{
			continue;
		}
		inside += hits[i].y;
		if (!isEntryPointSet)
		{
			entryPoint = hits[i].x;
			isEntryPointSet = true;
		}
		if (inside == 0.0f)
		{
			opticalDepth += hits[i].x - entryPoint;
			isEntryPointSet = false;
		}
	}
	//opticalDepths[DTid.x] = opticalDepth * 20.0f;
	opticalDepths[particleId] = opticalDepth * 50.0f - (50.0f - 1.0f);
}