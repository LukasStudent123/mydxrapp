#define RAYTRACER
#include "ShaderStructures.h"

RWTexture2D<float4> outTex : register(u0); // Output texture
RWStructuredBuffer<float> opticalDepths : register(u1); // Optical Depth buffer
RWStructuredBuffer<float> vecOpticalDepths : register(u2); // vec Optical Depth buffer
RaytracingAccelerationStructure sceneAccelStruct : register(t0, space0); // Scene Acceleration Structure
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<MyDXRApp::VertexPositionColor> Vertices : register(t2, space0);
Texture2D<float4> Tex : register (t3, space0);

ConstantBuffer<MyDXRApp::RayGenData> rayData : register(b0);
ConstantBuffer<MyDXRApp::ParticlePositionData> particlePosData : register(b1);
ConstantBuffer<MyDXRApp::ValuesData> valuesData : register(b2);

[numthreads(BLOCKSIZE, 1, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
	vecOpticalDepths[DTid.x] = 0.0f;
}