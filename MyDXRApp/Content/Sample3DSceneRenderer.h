﻿#pragma once

#include "..\Common\DeviceResources.h"
#include "ShaderStructures.h"
#include "..\Common\StepTimer.h"
#include <unordered_map>
#include "..\Common\DirectXHelper.h"
#include "..\Common\PerformanceTimers.h"
#include "..\Common\Icosahedron.h"
#include <ResourceUploadBatch.h>
#include <DDSTextureLoader.h>

#define RANDOM_PARTICLE_POS
//#define PARTICLE_MOVEMENT

#define BASE_VIEW
//#define VIEW1
//#define VIEW2

namespace MyDXRApp
{
	// This sample renderer instantiates a basic rendering pipeline.
	class Sample3DSceneRenderer
	{
	public:
		Sample3DSceneRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~Sample3DSceneRenderer();
		void CreateDeviceDependentResources();
		void CreateWindowSizeDependentResources();
		void Update(DX::StepTimer const& timer);
		bool Render();
		void SaveState();

		void StartTracking();
		void TrackingUpdate(float positionX);
		void StopTracking();
		bool IsTracking() { return m_tracking; }

		void HandleKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e);
		void HandleKeyUp(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e);

	private:
		void LoadState();
		void Rotate(float radians);
		void RotateLight(float radians);
		void RotateCamX(float radians);
		void RotateCamY(float radians);

		void CreateSphere(std::vector<VertexPositionColor> &sphereVertices, std::vector<unsigned short> &sphereIndices, float const radius, unsigned int const longitude_split_count, unsigned int const latitude_split_count, float const offset, XMFLOAT3 color);
		void CreateQuad(std::vector<VertexPositionColor>& quadVertices, std::vector<unsigned short>& quadIndices, float const width, float const height, unsigned int const horizontal_split_count, unsigned int const vertical_split_count, XMFLOAT3 offset, XMFLOAT3 color);
		void UpdateTopLevelAS();
		void CalculateFrameStats();
		void GenerateParticles();
		void CreateTexture();

	private:
		static const UINT FrameCount = DX::c_frameCount;

		// Constant buffers must be 256-byte aligned.
		//static const UINT c_alignedConstantBufferSize = (sizeof(MyDXRApp::ModelViewProjectionConstantBuffer) + 255) & ~255;
		static const UINT c_alignedConstantBufferSize = (sizeof(MyDXRApp::RayGenData) + 255) & ~255;

		union AlignedRayGenData {
			RayGenData data;
			uint8_t alignmentPadding[D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT];
		};

		static const UINT c_paddingSize = (sizeof(ParticlePositionData) + (D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT - 1)) & ~(D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT - 1);
		union AlignedParticlePositionData {
			ParticlePositionData data;
			uint8_t alignmentPadding[c_paddingSize];
		};

		static const UINT c_valuesPaddingSize = (sizeof(ValuesData) + (D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT - 1)) & ~(D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT - 1);
		union AlignedValuesData {
			ValuesData data;
			uint8_t alignmentPadding[c_paddingSize];
		};

		const wchar_t* c_raygenShaderName = L"RayGeneration";
		//const wchar_t* c_opticalDepthRaygenShaderName = L"RayGenerationOpticalDepth";
		const wchar_t* c_opticalDepthRaygenShaderName = L"RayGenerationOpticalDepth2";

		const wchar_t* c_closestHitShaderName = L"RayClosestHit";
		const wchar_t* c_missShaderName = L"RayMiss";
		const wchar_t* c_hitGroupName = L"MyHitGroup";
		const wchar_t* c_anyHitShaderName = L"RayAnyHit";

		const wchar_t* c_missOpticalDepthShaderName = L"RayMissOpticalDepth";
		const wchar_t* c_hitGroupOpticalDepthName = L"MyHitGroupOpticalDepth";
		const wchar_t* c_closestHitOpticalDepthShaderName = L"RayClosestHitOpticalDepth";
		const wchar_t* c_anyHitOpticalDepthShaderName = L"RayAnyHitOpticalDepth";

		const wchar_t* c_missShadowShaderName = L"RayMissShadow";
		const wchar_t* c_hitGroupShadowName = L"MyHitGroupShadow";
		const wchar_t* c_closestHitShadowShaderName = L"RayClosestHitShadow";

		bool	m_aKey = false;
		bool	m_sKey = false;
		bool	m_dKey = false;
		bool	m_wKey = false;
		bool	m_qKey = false;
		bool	m_eKey = false;
		bool	m_upKey = false;
		bool	m_downKey = false;
		bool	m_leftKey = false;
		bool	m_rightKey = false;
		bool	m_shouldLightRotate = true;

		bool m_basic_mode = false;
		bool m_simple_shadow_mode = false;
		bool m_full_optical_depth_mode = true;
		bool m_shouldTexFrameUpdate = true;

		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Direct3D resources for cube geometry.
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList4>	m_commandList;
		//Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_rootSignature;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_globalRootSignature;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_localRootSignature;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_pipelineState;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_computePipelineState;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_clearComputePipelineState;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbvHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;
		//MyDXRApp::ModelViewProjectionConstantBuffer			m_constantBufferData;
		MyDXRApp::RayGenData								m_constantBufferData[FrameCount];
		AlignedRayGenData*									m_mappedConstantBuffer;
		UINT												m_descriptorsAllocated;
		UINT												m_cbvDescriptorSize;
		D3D12_RECT											m_scissorRect;
		std::vector<byte>									m_vertexShader;
		std::vector<byte>									m_pixelShader;
		std::vector<byte>									m_rayTracer;
		std::vector<byte>									m_computeShader;
		std::vector<byte>									m_clearComputeShader;
		D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW								m_indexBufferView;

		Microsoft::WRL::ComPtr<ID3D12StateObject>			m_dxrStateObject;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_accelerationStructure;
		//Microsoft::WRL::ComPtr<ID3D12Resource>				m_bottomLevelAccelerationStructure;
		std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>>	m_bottomLevelAccelerationStructures;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_topLevelAccelerationStructure;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_raytracingOutput;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_raytracingOutputResourceUAVGpuDescriptor;
		UINT												m_raytracingOutputResourceUAVDescriptorHeapIndex;
		Microsoft::WRL::ComPtr<ID3D12Device5>				m_dxrDevice;
		D3D12_CPU_DESCRIPTOR_HANDLE							m_indexBufferResourceSRVCpuDescriptor;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_indexBufferResourceSRVGpuDescriptor;
		D3D12_CPU_DESCRIPTOR_HANDLE							m_vertexBufferResourceSRVCpuDescriptor;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_vertexBufferResourceSRVGpuDescriptor;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_texture;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_textureUploadResource;
		D3D12_CPU_DESCRIPTOR_HANDLE							m_textureResourceSRVCpuDescriptor;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_textureResourceSRVGpuDescriptor;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_texture2;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_textureUploadResource2;
		D3D12_CPU_DESCRIPTOR_HANDLE							m_textureResourceSRVCpuDescriptor2;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_textureResourceSRVGpuDescriptor2;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_instanceDescs;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_scratchResource;
		float												m_elapsed_seconds = 0.0f;

		std::vector<VertexPositionColor>					m_vertexBufferData;
		std::vector<unsigned short>							m_indexBufferData;

		std::vector<VertexPositionColor>					m_particleVertexBufferData;
		std::vector<unsigned short>							m_particleIndexBufferData;
		std::vector<VertexPositionColor>					m_quadParticleVertexBufferData;
		std::vector<unsigned short>							m_quadParticleIndexBufferData;
		std::vector<VertexPositionColor>					m_cubeVertexBufferData;
		std::vector<unsigned short>							m_cubeIndexBufferData;
		std::vector<VertexPositionColor>					m_quadVertexBufferData;
		std::vector<unsigned short>							m_quadIndexBufferData;

		const UINT											m_nbrGeometries = 4;

		std::vector<XMFLOAT3>								m_offsets;
		std::vector<XMFLOAT3>								m_motionVectors;
		UINT64												m_vertexIdx;
		UINT64												m_indexIdx;

		Microsoft::WRL::ComPtr<ID3D12Heap>					m_heap;

		const UINT64										m_nbrParticles = NBR_PARTICLES;
		const float											m_motionSpeed = 5.0f;
		std::vector<float>									m_motionVelocities;
		const float											m_motionLength = 1.0f / 50.0f;
		std::vector<float>									m_rotSpeeds;
		std::vector<float>									m_scalingSpeeds;

		const UINT											m_particleGroupAmount = PARTICLE_GROUP_AMOUNT;
		UINT												m_currentParticleGroup = 0;
		std::vector<XMFLOAT3>								m_particlePositions;
		XMFLOAT3											m_particleRot;
		UINT												m_texFrame;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_particleConstantBuffer;
		ParticlePositionData								m_particleConstantBufferData[FrameCount];
		AlignedParticlePositionData*						m_mappedParticleConstantBuffer;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_opticalDepthOutput;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_opticalDepthOutputResourceUAVGpuDescriptor;
		UINT												m_opticalDepthOutputResourceUAVDescriptorHeapIndex;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vecOpticalDepthOutput;
		D3D12_GPU_DESCRIPTOR_HANDLE							m_vecOpticalDepthOutputResourceUAVGpuDescriptor;
		UINT												m_vecOpticalDepthOutputResourceUAVDescriptorHeapIndex;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_valuesConstantBuffer;
		ValuesData											m_valuesConstantBufferData[FrameCount];
		AlignedValuesData*									m_mappedValuesConstantBuffer;

		DXTimer::GPUTimer									m_gpuTimer;

		struct ShaderTable
		{
			Microsoft::WRL::ComPtr<ID3D12Resource>			table;
			uint32_t										shaderTableRecordSize;
		} m_shaderTable;

		ShaderTable m_opticalDepthShaderTable;

		struct RayGenData
		{
			DirectX::XMVECTOR wsCamPos;
			DirectX::XMVECTOR wsCamU;
			DirectX::XMVECTOR wsCamV;
			DirectX::XMVECTOR wsCamW;
			DirectX::XMMATRIX projectionToWorld;
			DirectX::XMVECTOR lightPos;
			DirectX::XMVECTOR lightAmbientColor;
			DirectX::XMVECTOR lightDiffuseColor;
		} m_rayGenData;

		struct ParticlePositionData
		{
			//XMFLOAT3 positions[NBR_PARTICLES / PARTICLE_GROUP_AMOUNT];
			XMFLOAT3 positions[NBR_PARTICLES];
		} m_particlePositionData;

		struct ValuesData
		{
			DirectX::XMFLOAT3 particleRot;
			UINT currentParticleGroup;
			UINT texFrame;
			UINT basic_mode;
			UINT simple_shadow_mode;
			UINT full_optical_depth_mode;
		} m_valuesData;

		//CubeConstantBuffer m_cubeCB;

		bool	m_fileComplete;
		std::wstring	m_windowText;

		bool	m_fileComplete2;
		std::wstring	m_csvText;

		// Variables used with the rendering loop.
		bool	m_loadingComplete;
		float	m_radiansPerSecond;
		float	m_angle;
		bool	m_tracking;
	};
}

